﻿using System;
using CbirSystem.Base.Model.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CbirSystem.DAL.EF
{
    public class CbirDbContext<TUser, TRole, TKey> : IdentityDbContext<TUser, TRole, TKey>
          where TUser : IdentityUser<TKey>
          where TRole : IdentityRole<TKey>
          where TKey : IEquatable<TKey>
    {
        private readonly ConnectionStringDto _connectionStringDto;
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<FeatureEntity> Features { get; set; }

        public CbirDbContext(ConnectionStringDto connectionStringDto) :base()
        {
            _connectionStringDto = connectionStringDto;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_connectionStringDto.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
