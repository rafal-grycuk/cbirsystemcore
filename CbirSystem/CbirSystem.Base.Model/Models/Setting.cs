﻿using System.ComponentModel.DataAnnotations;

namespace CbirSystem.Base.Model.Models
{
    public class Setting
    {
        [Key]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
