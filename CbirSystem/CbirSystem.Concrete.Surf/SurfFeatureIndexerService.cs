﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accord.Imaging;
using Accord.MachineLearning;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using CbirSystem.Utilities;
using DataAccessLayer.Core.Interfaces.UoW;

namespace CbirSystem.Concrete.Surf
{
    public class SurfFeatureIndexerService : AbstractFeatureIndexerService<SpeededUpRobustFeaturePoint>
    {
        private readonly IDictionary<Base.Model.Models.Image, Dictionary<int?, double>> _histogramsDictionary;
        private readonly EditableKeyValuePair<int, double[][]> _clusters;
        private readonly IUnitOfWork _uow;
        public SurfFeatureIndexerService(IFeatureExtractorService<SpeededUpRobustFeaturePoint> extractor,
                                         IDictionary<Base.Model.Models.Image, Dictionary<int?, double>> histogramsDictionary,
                                         EditableKeyValuePair<int, double[][]> clusters,
                                         IUnitOfWork uow) : base(extractor)
        {
            _uow = uow;
            _clusters = clusters;
            _histogramsDictionary = histogramsDictionary;
        }
        public override async Task CreateIndex(params object[] parameters)
        {
            await Task.Run(() =>
            {
                var images = _uow.Repository<Base.Model.Models.Image>().GetRange();
                if (parameters != null && parameters.Any() && parameters[0] != null &&
                    int.TryParse(parameters[0].ToString(), out int featuresClustersCount))
                {
                }
                else
                    featuresClustersCount = images.GroupBy(x => x.Tag).Count();
                var featuresEntities = _uow.Repository<FeatureEntity>()
                    .GetRange(filterPredicate: x => x.IndexerType == Indexer.Surf).ToList();
                var features = SurfFeatureExtractorService.ToFeatureMatrix(featuresEntities);
                double[][] featureMatrix = (double[][])SurfFeatureExtractorService.GetFeatureMatrix(features);
                KMeans clustetingFeatures = new KMeans(k: featuresClustersCount);
                var clusterCollection = clustetingFeatures.Learn(featureMatrix);
                int[] groupsLabelsFeatures = clusterCollection.Decide(featureMatrix);
                for (int i = 0; i < groupsLabelsFeatures.Length; i++)
                    featuresEntities[i].GroupId = groupsLabelsFeatures[i];
                _uow.Save();

                var featuresGroupedByImageId = featuresEntities
                    .GroupBy(f => f.ImageId)
                    .Select(f => new { ImageId = f.Key, Features = f.ToList() })
                    .Join(images, feature => feature.ImageId, image => image.Id,
                        (feat, image) => new { Image = image, feat.Features })
                    .ToDictionary(f => f.Image, f => f.Features);

                var histogramsDictionary = featuresGroupedByImageId
                    .Select(fg => new
                    {
                        ImageId = fg.Key,
                        Histogram = fg.Value.GroupBy(f => f.GroupId)
                            .Select(x => new { GroupId = x.Key, FeaturesCount = (double)x.Count() })
                            .OrderBy(x => x.GroupId)
                            .ToDictionary(x => x.GroupId, x => x.FeaturesCount)
                    }).ToDictionary(x => x.ImageId, x => x.Histogram);

                var maxGroup = histogramsDictionary.Max(x => x.Value.Max(y => y.Key));
                foreach (var hist in histogramsDictionary)
                {
                    for (int i = 0; i <=maxGroup; i++)
                    {
                        if (!hist.Value.ContainsKey(i))
                            hist.Value.Add(i, 0);
                    }
                    _histogramsDictionary.Add(hist);
                }
                _clusters.Key = featuresClustersCount;
                _clusters.Value = clusterCollection.Centroids;
            });
        }

        public override void DeleteIndex()
        {
            var featureMatrix = _uow.Repository<FeatureEntity>().GetRange(filterPredicate: x => x.IndexerType == Indexer.Surf);
            _uow.Repository<FeatureEntity>().DeleteRange(featureMatrix);
            _uow.Save();
        }


    }
}
