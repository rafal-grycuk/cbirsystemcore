﻿using Accord.Imaging;
using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.Extensions.Logging;
using CbirSystem.Base.Logic;

namespace CbirSystem.Api.Controllers
{

    public class SurfIndexerController : IndexerController<SpeededUpRobustFeaturePoint>
    {
        public SurfIndexerController(IUnitOfWork uow,
                                     ILoggerFactory loggerFactory,
                                     AbstractExecutorService<SpeededUpRobustFeaturePoint> executor) : base(uow, loggerFactory, executor) { }
    }
}