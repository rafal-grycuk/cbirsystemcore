﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CbirSystem.VisualizationEndpoint
{
    public partial class ChartForm : Form
    {
        public Chart Chart
        {
            get
            {
                return _chart;
            }

            set
            {
                _chart = value;
            }
        }
        public ChartForm()
        {
            InitializeComponent();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _saveFileDialogToImage.FileName = "simulation";
            _saveFileDialogToImage.ShowDialog();
        }

        private void _saveFileDialogToImage_FileOk(object sender, CancelEventArgs e)
        {
            ChartImageFormat format = _saveFileDialogToImage.FilterIndex == 0 ? ChartImageFormat.Jpeg : ChartImageFormat.Png;              
            Chart.SaveImage(_saveFileDialogToImage.FileName, format);
        }
    }
}
