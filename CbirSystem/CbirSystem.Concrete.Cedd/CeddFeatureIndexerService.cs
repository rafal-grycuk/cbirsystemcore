﻿using System.Linq;
using System.Threading.Tasks;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using CbirSystem.Concrete.Cedd.CEDD;
using DataAccessLayer.Core.Interfaces.UoW;

namespace CbirSystem.Concrete.Cedd
{
    public class CeddFeatureIndexerService : AbstractFeatureIndexerService<double[]>
    {
        private readonly IUnitOfWork _uow;
        private readonly BkTree<CeddTreeNode> _ceddTree;
        public BkTree<CeddTreeNode> CeddTree => _ceddTree;
        public CeddFeatureIndexerService(IFeatureExtractorService<double[]> extractor, IUnitOfWork uow, BkTree<CeddTreeNode> ceddtree) : base(extractor)
        {
            _uow = uow;
            _ceddTree = ceddtree;
        }

        public override async Task CreateIndex(params object[] param)
        {
            await Task.Run(() =>
            {
                if (_ceddTree.Count == 0)
                {
                    var featuresEntities = _uow.Repository<FeatureEntity>().GetRange(filterPredicate: x => x.IndexerType == Indexer.Cedd, enableTracking: false).ToList();
                    var features = CeddFeatureExtractorService.ToFeatureMatrix(featuresEntities);

                    features.ToList().ForEach(feature =>
                    {
                        CeddTreeNode ceddTreeNode = new CeddTreeNode
                        {
                            CeddDiscriptor = feature.Data,
                            Id = feature.ImageId
                        };
                        _ceddTree.Add(ceddTreeNode);
                    });
                }
            });
        }

        public override void DeleteIndex()
        {
            var featureMatrix = _uow.Repository<FeatureEntity>().GetRange(filterPredicate: x => x.IndexerType == Indexer.Cedd);
            _uow.Repository<FeatureEntity>().DeleteRange(featureMatrix);
            _uow.Save();
            _ceddTree.Clear();
        }
    }
}
