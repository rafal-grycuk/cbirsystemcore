﻿using System.ComponentModel.DataAnnotations;

namespace CbirSystem.Dto
{
    public class UserToRoleDto
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Role { get; set; }
    }
}
