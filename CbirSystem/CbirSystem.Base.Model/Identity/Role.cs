﻿using Microsoft.AspNetCore.Identity;

namespace CbirSystem.Base.Model.Identity
{
    public class Role : IdentityRole<int>
    {
        public Role() { }
        public Role(string name) : base(name) { }
    }
}
