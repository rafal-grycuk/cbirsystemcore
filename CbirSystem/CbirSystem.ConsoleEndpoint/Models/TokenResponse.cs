﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace CbirSystem.ConsoleEndpoint.Models
{
    public class TokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn{ get; set; }
    }
}
