﻿using CbirSystem.Base.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.Core.Interfaces.UoW;

namespace CbirSystem.Base.Logic
{

    public class SimulationEvaluatorService : ISimulationEvaluatorService<RetrievalFactors>
    {
        private readonly IExecutorService _executor;
        private readonly IUnitOfWork _uow;
        public SimulationEvaluatorService(IExecutorService executor, IUnitOfWork uow)
        {
            _executor = executor;
            _uow = uow;
        }

        public IEnumerable<RetrievalFactors> SimulateMultiQuery(IEnumerable<Image> queries, params object[] parameters)
        {
            var retrievalFactorsFromMultiQuery = new List<RetrievalFactors>();
            foreach (var query in queries)
            {
                RetrievalFactors factors = SimulateSingleQuery(query, parameters);
                retrievalFactorsFromMultiQuery.Add(factors);
            }
            return retrievalFactorsFromMultiQuery;
        }

        public RetrievalFactors SimulateSingleQuery(Image query, params object[] parameters)
        {
            var images = _uow.Repository<Image>().GetRange(enableTracking: false).ToList();
            var retrivedImages = _executor.ExecuteQuery(query, parameters); // RI
            var appropriate = images.Where(image => image.Tag == query.Tag).ToList(); // AI
            var correctRetrivedImages = retrivedImages.Where(f => appropriate.Any(b => b.Tag == f.Tag))
                .ToList(); // RAI 
            var inappropriateReturnedImages = retrivedImages
                .Where(f => !correctRetrivedImages.Any(b => b.Tag == f.Tag)).ToList(); // IRI = RI - RAI
            var appropriateNotReturned = appropriate.Count - correctRetrivedImages.Count;
            double precision = Math.Round(((double)Math.Abs(correctRetrivedImages.Count()) /
                                           Math.Abs((correctRetrivedImages.Count + inappropriateReturnedImages.Count))) *
                                          100);
            double recall = Math.Round(((double)Math.Abs(correctRetrivedImages.Count()) /
                                        Math.Abs((correctRetrivedImages.Count() + appropriateNotReturned))) * 100);
            var result = new RetrievalFactors()
            {
                RetrivedImages = retrivedImages.Count(),
                AppropriateImages = appropriate.Count(),
                CorrectRetrivedImages = correctRetrivedImages.Count(),
                InappropriateReturnedImages = inappropriateReturnedImages.Count,
                AppropriateNotReturned = appropriateNotReturned,
                QueryImageName = query.ImageName,
                QueryImageTag = query.Tag,
                Precision = double.IsNaN(precision) ? 0 : precision,
                Recall = double.IsNaN(recall) ? 0 : recall
            };
            return result;
        }

        public void CreateIndex(params object[] parameters)
        {
            _executor.CreateIndex(parameters);
        }

        public void DeleteIndex()
        {
            _executor.DeleteIndex();
        }
    }
}
