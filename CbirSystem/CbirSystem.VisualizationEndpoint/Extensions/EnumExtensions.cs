﻿using System;
using System.Reflection;
using CbirSystem.VisualizationEndpoint.Filters;

namespace CbirSystem.VisualizationEndpoint.Extensions
{
    public static class EnumExtensions
    {
        public static string GetPath(Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null)
                return null;
            var attribute = (PathInfo)fieldInfo.GetCustomAttribute(typeof(PathInfo));
            return attribute.Path;
        }
    }
}
