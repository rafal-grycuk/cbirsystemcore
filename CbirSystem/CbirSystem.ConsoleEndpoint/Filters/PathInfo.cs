﻿using System;

namespace CbirSystem.ConsoleEndpoint.Filters
{
    public class PathInfo : Attribute
    {
        public string Path { get; set; }

        public PathInfo(string path)
        {
            string basePath = AppContext.BaseDirectory;
#if DEBUG
            basePath = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("CbirSystem\\", StringComparison.Ordinal));
#endif
            Path = basePath + path;
        }
    }
}
