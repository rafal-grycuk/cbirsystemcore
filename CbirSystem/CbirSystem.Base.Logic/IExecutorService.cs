﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CbirSystem.Base.Model.Models;

namespace CbirSystem.Base.Logic
{
    public interface IExecutorService
    {
        IList<Image> ExecuteQuery(Image query, params object[] parameters);
        Task CreateIndex(params object[] parameters);
        void DeleteIndex();
    }
}
