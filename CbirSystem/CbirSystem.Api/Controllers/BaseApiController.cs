﻿using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CbirSystem.Api.Controllers
{
    [Authorize]
    //[EnableCors("AllowCors")]
    [Route("[controller]")]
    public abstract class BaseApiController : Controller
    {
        protected readonly IUnitOfWork _uow;
        protected readonly ILogger _logger;
        protected BaseApiController(IUnitOfWork uow, ILoggerFactory loggerFactory)
        {
            _uow = uow;
            _logger = loggerFactory.CreateLogger<BaseApiController>();
        }

        protected void AddErrors(IdentityResult result, string validatorName = "")
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(validatorName, error.Description);
            }
        }
    }
}
