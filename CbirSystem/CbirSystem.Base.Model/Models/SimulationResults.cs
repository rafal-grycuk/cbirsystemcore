﻿using System.Collections.Generic;
using System.Linq;

namespace CbirSystem.Base.Model.Models
{
    public class SimulationResults
    {
        public IEnumerable<RetrievalFactors> RetrievalFactors { get; set; }
        public double AvgPrecision { get; set; }
        public double AvgRecall { get; set; }

        public override string ToString()
        {
            string str = "=====RESULTS=====" + "\n\r";
            foreach (var rf in RetrievalFactors)
            {

                str += rf.ToString() + "\n\r";
            }
            str += "=================================== \n\r" +
            "Avg Precision: " + AvgPrecision + "\n\r" +
            "Avg Recall: " + AvgRecall;
            return str;
        }
    }
}
