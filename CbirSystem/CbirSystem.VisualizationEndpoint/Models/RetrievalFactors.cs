﻿namespace CbirSystem.VisualizationEndpoint.Models
{
    public class RetrievalFactors
    {
        public double RetrivedImages { get; set; }

        public double AppropriateImages { get; set; }

        public double CorrectRetrivedImages { get; set; }

        public double InappropriateReturnedImages { get; set; }

        public double AppropriateNotReturned { get; set; }

        public double Precision { get; set; }

        public double Recall { get; set; }

        public string QueryImageName { get; set; }

        public string QueryImageTag { get; set; }

        public override string ToString()
        {
            string str = "QueryImageName (classId): " + QueryImageName + "(" + QueryImageTag + ")" + "          | RI: " + RetrivedImages + " | AI: " + AppropriateImages + " | CRI: " + CorrectRetrivedImages + " | IRI: " + InappropriateReturnedImages + " | ANR: " + AppropriateNotReturned + " | Precision: " + Precision + " | Recall: " + Recall;
            return str;
        }

        public string ToLatex()
        {
            string latexStr = @"\hline" + "\r\n";
            latexStr += QueryImageName + "(" + QueryImageTag + ") & " +
                        RetrivedImages + " & " +
                        AppropriateImages + " & " +
                        CorrectRetrivedImages + " & " +
                        InappropriateReturnedImages + " & " +
                        AppropriateNotReturned + " & " +
                        Precision + " & " +
                        Recall + " \\\\ " + "\r\n";
            return latexStr;
        }

        public string ToExcel()
        {
            string excelStr = RetrivedImages + "\t" +
                              AppropriateImages + "\t" +
                              CorrectRetrivedImages + "\t" +
                              InappropriateReturnedImages + "\t" +
                              AppropriateNotReturned;
            return excelStr;
        }
    }
}
