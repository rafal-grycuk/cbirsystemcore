﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CbirSystem.Base.Model.Models;

namespace CbirSystem.Base.Logic
{
    public interface IFeatureExtractorService<TFeatureType>
    {
        Task<IList<Feature<TFeatureType>>> ExtractFeatures(Image image);
        Task<IList<Feature<TFeatureType>>> ExtractAllFeatures(IList<Image> images);

    }
}
