﻿using CbirSystem.Base.Model.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace CbirSystem.DAL.EF
{
    public static class Seed
    {
        public static async void RunSeed(DbContext context, RoleManager<Role> roleManager, UserManager<User> userManager)
        {
            Role adminRole = new Role()
            {
                Name = "Administrator"
            };
            if (await roleManager.RoleExistsAsync(adminRole.Name) == false)
                await roleManager.CreateAsync(adminRole);

            User admin = new User()
            {
                UserName = "Admin"
            };
            var result = await userManager.CreateAsync(admin, "Admin1234");
            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(admin, "Administrator");
            }
        }

    }
}
