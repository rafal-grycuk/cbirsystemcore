﻿using System.Collections.Generic;
using CbirSystem.Base.Model.Models;

namespace CbirSystem.Base.Logic
{
    public interface ISimulationEvaluatorService<out TResultMeasureType>
    {
        IEnumerable<TResultMeasureType> SimulateMultiQuery(IEnumerable<Image> queries, params object[] parameters);
        TResultMeasureType SimulateSingleQuery(Image query, params object[] parameters);
        void CreateIndex(params object[] parameters);
        void DeleteIndex();
    }
}
