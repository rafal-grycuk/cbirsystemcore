﻿using System;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    class Fuzzy24Bin
    {
        public struct FuzzyRules
        {
            public int Input1 { get; set; }
            public int Input2 { get; set; }
            public int Output { get; set; }

        }
        public double[] ResultsTable = new double[3];
        public double[] Fuzzy24BinHisto = new double[24];
        public bool KeepPreviusValues;
        public FuzzyRules[] Fuzzy24BinRules = new FuzzyRules[4];
        public double[] SaturationActivation = new double[2];
        public double[] ValueActivation = new double[2];
        public int[,] Fuzzy24BinRulesDefinition = {
            {1,1,1},
            {0,0,2},
            {0,1,0},
            {1,0,2}
        };

        protected double[] SaturationMembershipValues = new double[8] {  0,0,68, 188,
            68,188,255, 255};

        protected double[] ValueMembershipValues = new double[8] {  0,0,68, 188,
            68,188,255, 255};


        public Fuzzy24Bin(bool keepPreviuesValues)
        {
            for (int R = 0; R < 4; R++)
            {

                Fuzzy24BinRules[R].Input1 = Fuzzy24BinRulesDefinition[R, 0];
                Fuzzy24BinRules[R].Input2 = Fuzzy24BinRulesDefinition[R, 1];
                Fuzzy24BinRules[R].Output = Fuzzy24BinRulesDefinition[R, 2];
            }
            KeepPreviusValues = keepPreviuesValues;
        }

        private void FindMembershipValueForTriangles(double input, double[] triangles, double[] membershipFunctionToSave)
        {
            int temp = 0;
            for (int i = 0; i <= triangles.Length - 1; i += 4)
            {
                membershipFunctionToSave[temp] = 0;
                if (input >= triangles[i + 1] && input <= +triangles[i + 2])
                {
                    membershipFunctionToSave[temp] = 1;
                }
                if (input >= triangles[i] && input < triangles[i + 1])
                {
                    membershipFunctionToSave[temp] = (input - triangles[i]) / (triangles[i + 1] - triangles[i]);
                }
                if (input > triangles[i + 2] && input <= triangles[i + 3])
                {
                    membershipFunctionToSave[temp] = (input - triangles[i + 2]) / (triangles[i + 2] - triangles[i + 3]) + 1;
                }
                temp += 1;
            }
        }

        private void LomDefazzificator(FuzzyRules[] rules, double[] input1, double[] input2, double[] resultTable)
        {
            int ruleActivation = -1;
            double lomMaxOfMin = 0;
            for (int i = 0; i < rules.Length; i++)
            {
                if ((input1[rules[i].Input1] > 0) && (input2[rules[i].Input2] > 0))
                {
                    double min = 0;
                    min = Math.Min(input1[rules[i].Input1], input2[rules[i].Input2]);
                    if (min > lomMaxOfMin)
                    {
                        lomMaxOfMin = min;
                        ruleActivation = rules[i].Output;
                    }
                }
            }
            resultTable[ruleActivation]++;
        }

        private void MultiParticipateEqualDefazzificator(FuzzyRules[] rules, double[] input1, double[] input2, double[] resultTable)
        {
            int ruleActivation = -1;
            for (int i = 0; i < rules.Length; i++)
            {
                if ((input1[rules[i].Input1] > 0) && (input2[rules[i].Input2] > 0))
                {
                    ruleActivation = rules[i].Output;
                    resultTable[ruleActivation]++;
                }
            }
        }

        private void MultiParticipateDefazzificator(FuzzyRules[] rules, double[] input1, double[] input2, double[] resultTable)
        {
            for (int i = 0; i < rules.Length; i++)
            {
                if ((input1[rules[i].Input1] > 0) && (input2[rules[i].Input2] > 0))
                {
                    var min = Math.Min(input1[rules[i].Input1], input2[rules[i].Input2]);
                    var ruleActivation = rules[i].Output;
                    resultTable[ruleActivation] += min;
                }
            }
        }

        public double[] ApplyFilter(double hue, double saturation, double value, double[] colorValues, int method)
        {
            ResultsTable[0] = 0;
            ResultsTable[1] = 0;
            ResultsTable[2] = 0;
            double temp = 0;
            FindMembershipValueForTriangles(saturation, SaturationMembershipValues, SaturationActivation);
            FindMembershipValueForTriangles(value, ValueMembershipValues, ValueActivation);
            if (this.KeepPreviusValues == false)
            {
                for (int i = 0; i < 24; i++)
                {
                    Fuzzy24BinHisto[i] = 0;
                }
            }

            for (int i = 3; i < 10; i++)
            {
                temp += colorValues[i];
            }

            if (temp > 0)
            {
                if (method == 0) LomDefazzificator(Fuzzy24BinRules, SaturationActivation, ValueActivation, ResultsTable);
                if (method == 1) MultiParticipateEqualDefazzificator(Fuzzy24BinRules, SaturationActivation, ValueActivation, ResultsTable);
                if (method == 2) MultiParticipateDefazzificator(Fuzzy24BinRules, SaturationActivation, ValueActivation, ResultsTable);
            }

            for (int i = 0; i < 3; i++)
            {
                Fuzzy24BinHisto[i] += colorValues[i];
            }
            
            for (int i = 3; i < 10; i++)
            {
                Fuzzy24BinHisto[(i - 2) * 3] += colorValues[i] * ResultsTable[0];
                Fuzzy24BinHisto[(i - 2) * 3 + 1] += colorValues[i] * ResultsTable[1];
                Fuzzy24BinHisto[(i - 2) * 3 + 2] += colorValues[i] * ResultsTable[2];
            }

            return (Fuzzy24BinHisto);
        }
    }
}