﻿using System.Collections.Generic;
using System.Linq;

namespace CbirSystem.VisualizationEndpoint.Models
{
    public class SimulationResults
    {
        public IEnumerable<RetrievalFactors> RetrievalFactors { get; set; }
        public double AvgPrecision { get; set; }
        public double AvgRecall { get; set; }
        public override string ToString()
        {
            string str = "=====RESULTS=====" + "\n\r" +
                         RetrievalFactors.Select(rf => rf.ToString()) + "\n\r" +
                         "Avg Precision: " + AvgPrecision + "\n\r" +
                         "Avg Recall: " + AvgRecall;
            return str;
        }
    }
}
