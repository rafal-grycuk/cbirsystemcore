﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CbirSystem.Utilities
{
    public class EditableKeyValuePair<TKey,TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }

        public EditableKeyValuePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public EditableKeyValuePair()
        {
            
        }
    }
}
