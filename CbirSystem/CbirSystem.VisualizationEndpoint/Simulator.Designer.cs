﻿namespace CbirSystem.VisualizationEndpoint
{
    partial class Simulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 1D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(4D, 0D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint7 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 0D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint8 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(2D, 2D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(3D, 1D);
            this.chartFactors = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showLatexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.biggerLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallerLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableDotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableDotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chartClassFactors = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chartPrecRecall = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBoxExcel = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chartFactors)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartClassFactors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPrecRecall)).BeginInit();
            this.SuspendLayout();
            // 
            // chartFactors
            // 
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            chartArea1.AxisX.MinorGrid.Interval = 1D;
            chartArea1.AxisX.Title = "Image Number";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            chartArea1.AxisY.Title = "Measure Value";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            chartArea1.Name = "ChartArea1";
            this.chartFactors.ChartAreas.Add(chartArea1);
            this.chartFactors.DataSource = this.chartFactors.Legends;
            this.chartFactors.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            this.chartFactors.Legends.Add(legend1);
            this.chartFactors.Location = new System.Drawing.Point(0, 0);
            this.chartFactors.Name = "chartFactors";
            this.chartFactors.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.BorderWidth = 3;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.MarkerBorderWidth = 10;
            series1.MarkerSize = 2;
            series1.MarkerStep = 2;
            series1.Name = "Series1";
            dataPoint1.BorderWidth = 2;
            dataPoint1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            dataPoint2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            dataPoint3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            series1.Points.Add(dataPoint3);
            this.chartFactors.Series.Add(series1);
            this.chartFactors.Size = new System.Drawing.Size(568, 174);
            this.chartFactors.TabIndex = 0;
            this.chartFactors.Text = "chart1";
            this.chartFactors.Click += new System.EventHandler(this.chartFactors_Click);
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOutput.Location = new System.Drawing.Point(9, 505);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutput.Size = new System.Drawing.Size(722, 102);
            this.textBoxOutput.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1159, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showLatexToolStripMenuItem,
            this.biggerLinesToolStripMenuItem,
            this.smallerLinesToolStripMenuItem,
            this.enableDotsToolStripMenuItem,
            this.disableDotsToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // showLatexToolStripMenuItem
            // 
            this.showLatexToolStripMenuItem.Name = "showLatexToolStripMenuItem";
            this.showLatexToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.showLatexToolStripMenuItem.Text = "Show Latex";
            this.showLatexToolStripMenuItem.Click += new System.EventHandler(this.showLatexToolStripMenuItem_Click);
            // 
            // biggerLinesToolStripMenuItem
            // 
            this.biggerLinesToolStripMenuItem.Name = "biggerLinesToolStripMenuItem";
            this.biggerLinesToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.biggerLinesToolStripMenuItem.Text = "Bigger Lines";
            this.biggerLinesToolStripMenuItem.Click += new System.EventHandler(this.biggerLinesToolStripMenuItem_Click);
            // 
            // smallerLinesToolStripMenuItem
            // 
            this.smallerLinesToolStripMenuItem.Name = "smallerLinesToolStripMenuItem";
            this.smallerLinesToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.smallerLinesToolStripMenuItem.Text = "Smaller Lines";
            this.smallerLinesToolStripMenuItem.Click += new System.EventHandler(this.smallerLinesToolStripMenuItem_Click);
            // 
            // enableDotsToolStripMenuItem
            // 
            this.enableDotsToolStripMenuItem.Name = "enableDotsToolStripMenuItem";
            this.enableDotsToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.enableDotsToolStripMenuItem.Text = "Enable Dots";
            this.enableDotsToolStripMenuItem.Click += new System.EventHandler(this.enableDotsToolStripMenuItem_Click);
            // 
            // disableDotsToolStripMenuItem
            // 
            this.disableDotsToolStripMenuItem.Name = "disableDotsToolStripMenuItem";
            this.disableDotsToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.disableDotsToolStripMenuItem.Text = "Disable Dots";
            this.disableDotsToolStripMenuItem.Click += new System.EventHandler(this.disableDotsToolStripMenuItem_Click);
            // 
            // chartClassFactors
            // 
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            chartArea2.AxisX.Title = "Class Tag";
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            chartArea2.AxisY.IsLabelAutoFit = false;
            chartArea2.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            chartArea2.AxisY.Title = "Avg Precision (%)";
            chartArea2.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            chartArea2.Name = "ChartArea1";
            this.chartClassFactors.ChartAreas.Add(chartArea2);
            this.chartClassFactors.DataSource = this.chartClassFactors.Legends;
            this.chartClassFactors.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            legend2.IsTextAutoFit = false;
            legend2.Name = "Legend1";
            this.chartClassFactors.Legends.Add(legend2);
            this.chartClassFactors.Location = new System.Drawing.Point(0, 0);
            this.chartClassFactors.Name = "chartClassFactors";
            this.chartClassFactors.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            series2.Legend = "Legend1";
            series2.Name = "Seri";
            series2.Points.Add(dataPoint4);
            series2.Points.Add(dataPoint5);
            series2.Points.Add(dataPoint6);
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chartClassFactors.Series.Add(series2);
            this.chartClassFactors.Size = new System.Drawing.Size(572, 174);
            this.chartClassFactors.TabIndex = 2;
            this.chartClassFactors.Text = "chart1";
            this.chartClassFactors.Click += new System.EventHandler(this.chartClassFactors_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(8, 34);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chartFactors);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chartClassFactors);
            this.splitContainer1.Size = new System.Drawing.Size(1143, 174);
            this.splitContainer1.SplitterDistance = 568;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 3;
            // 
            // chartPrecRecall
            // 
            this.chartPrecRecall.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            chartArea3.AxisX.MaximumAutoSize = 100F;
            chartArea3.AxisX.MinorGrid.Interval = 1D;
            chartArea3.AxisX.Title = "Recall";
            chartArea3.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            chartArea3.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            chartArea3.AxisY.MaximumAutoSize = 100F;
            chartArea3.AxisY.Title = "Precision";
            chartArea3.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            chartArea3.Name = "ChartArea1";
            this.chartPrecRecall.ChartAreas.Add(chartArea3);
            this.chartPrecRecall.DataSource = this.chartPrecRecall.Legends;
            legend3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            legend3.IsTextAutoFit = false;
            legend3.Name = "Legend1";
            this.chartPrecRecall.Legends.Add(legend3);
            this.chartPrecRecall.Location = new System.Drawing.Point(9, 214);
            this.chartPrecRecall.Name = "chartPrecRecall";
            this.chartPrecRecall.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series3.BorderWidth = 3;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.MarkerBorderWidth = 10;
            series3.MarkerSize = 2;
            series3.MarkerStep = 2;
            series3.Name = "Series1";
            dataPoint7.BorderWidth = 2;
            dataPoint7.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Square;
            dataPoint8.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            dataPoint9.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series3.Points.Add(dataPoint7);
            series3.Points.Add(dataPoint8);
            series3.Points.Add(dataPoint9);
            this.chartPrecRecall.Series.Add(series3);
            this.chartPrecRecall.Size = new System.Drawing.Size(1142, 285);
            this.chartPrecRecall.TabIndex = 4;
            this.chartPrecRecall.Text = "chart1";
            this.chartPrecRecall.Click += new System.EventHandler(this.chartPrecRecall_Click);
            // 
            // textBoxExcel
            // 
            this.textBoxExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxExcel.Location = new System.Drawing.Point(737, 505);
            this.textBoxExcel.Multiline = true;
            this.textBoxExcel.Name = "textBoxExcel";
            this.textBoxExcel.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExcel.Size = new System.Drawing.Size(414, 102);
            this.textBoxExcel.TabIndex = 5;
            // 
            // Simulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 615);
            this.Controls.Add(this.textBoxExcel);
            this.Controls.Add(this.chartPrecRecall);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Simulator";
            this.Text = "Simulator";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Simulator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartFactors)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartClassFactors)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPrecRecall)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartFactors;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showLatexToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartClassFactors;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem biggerLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallerLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableDotsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableDotsToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPrecRecall;
        private System.Windows.Forms.TextBox textBoxExcel;
    }
}

