﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CbirSystem.Base.Model.Models
{
    public class Image
    {
        #region Properties
        [Key]
        public int Id { get; set; }

        [Required]
        public byte[] RawImage { get; set; }
        [Required]
        public string ImageName { get; set; }
        [Required]
        public string Extension { get; set; }
        [Required]
        public string Tag { get; set; }

        [NotMapped]
        public double? Distance { get; set; }

        public ICollection<FeatureEntity> Features { get; set; }

        #endregion

        #region StaticMethods
        public static byte[] ImageToByte(System.Drawing.Bitmap bitmap)
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            return ms.ToArray();
        }
        public static System.Drawing.Bitmap ImageFromBytes(byte[] imageBytes)
        {
            return Accord.Imaging.Image.Clone(imageBytes);
        }
        #endregion

        #region Methods
        public Image() { }

        public Image(Image image)
        {
            Extension = image.Extension;
            Id = image.Id;
            ImageName = image.ImageName;
            RawImage = image.RawImage;
            Tag = image.Tag;
        }

        public static implicit operator System.Drawing.Bitmap(Image image)
        {
            return ImageFromBytes(image.RawImage);
        }
        #endregion
    }
}
