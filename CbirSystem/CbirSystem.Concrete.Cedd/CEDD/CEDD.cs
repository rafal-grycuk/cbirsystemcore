﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    public class Cedd
    {
        public struct MaskResults
        {
            public double Mask1 { get; set; }
            public double Mask2 { get; set; }
            public double Mask3 { get; set; }
            public double Mask4 { get; set; }
            public double Mask5 { get; set; }

        }

        public struct Neighborhood
        {
            public double Area1 { get; set; }
            public double Area2 { get; set; }
            public double Area3 { get; set; }
            public double Area4 { get; set; }
        }

        public double T0 { get; set; }
        public double T1 { get; set; }
        public double T2 { get; set; }
        public double T3 { get; set; }
        public bool Compact { get; set; }

        public Cedd(double th0, double th1, double th2, double th3, bool compactDescriptor)
        {
            T0 = th0;
            T1 = th1;
            T2 = th2;
            T3 = th3;
            Compact = compactDescriptor;
        }

        public Cedd()
        {
            T0 = 14;
            T1 = 0.68;
            T2 = 0.98;
            T3 = 0.98;
        }

        // Extract the descriptor
        public double[] Apply(Bitmap srcImg)
        {
            Fuzzy10Bin fuzzy10 = new Fuzzy10Bin(false);
            Fuzzy24Bin fuzzy24 = new Fuzzy24Bin(false);
            RgbToHsv hsvConverter = new RgbToHsv();
            int[] hsv = new int[3];
            double[] fuzzy10BinResultTable = new double[10];
            double[] fuzzy24BinResultTable = new double[24];
            double[] cedd = new double[144];
            int width = srcImg.Width;
            int height = srcImg.Height;
            double[,] imageGrid = new double[width, height];
            double[,] pixelCount = new double[2, 2];
            int[,] imageGridRed = new int[width, height];
            int[,] imageGridGreen = new int[width, height];
            int[,] imageGridBlue = new int[width, height];
            int numberOfBlocks = 1600; // blocks
            int stepX = (int)Math.Floor(width / Math.Sqrt(numberOfBlocks));
            int stepY = (int)Math.Floor(height / Math.Sqrt(numberOfBlocks));

            if ((stepX % 2) != 0)
            {
                stepX = stepX - 1;
            }
            if ((stepY % 2) != 0)
            {
                stepY = stepY - 1;
            }

            if (stepY < 2) stepY = 2;
            if (stepX < 2) stepX = 2;
            int[] edges = new int[6];

            MaskResults maskValues = new MaskResults();
            Neighborhood pixelsNeighborhood = new Neighborhood();
            PixelFormat fmt = (srcImg.PixelFormat == PixelFormat.Format8bppIndexed) ?
                PixelFormat.Format8bppIndexed : PixelFormat.Format24bppRgb;
            for (int i = 0; i < 144; i++)
                cedd[i] = 0;

            //****************
            //Incase below unsafe code gives error, uncomment the slow GetPixel code and 
            //comment the unsafe code
            //****************
            //for (int y = 0; y < height; y++)
            //{
            //    for (int x = 0; x < width; x++)
            //    {
            //        byte red = srcImg.GetPixel(x, y).R;
            //        byte green = srcImg.GetPixel(x, y).G;
            //        byte blue = srcImg.GetPixel(x, y).B;
            //        ImageGrid[x, y] = (0.299f * red + 0.587f * green + 0.114f * red);
            //        ImageGridRed[x, y] = (int)red;
            //        ImageGridGreen[x, y] = (int)green;
            //        ImageGridBlue[x, y] = (int)blue;
            //    }
            //}
            BitmapData srcData = srcImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, fmt);
            int offset = srcData.Stride - ((fmt == PixelFormat.Format8bppIndexed) ? width : width * 3);
            unsafe
            {
                byte* src = (byte*)srcData.Scan0.ToPointer();
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++, src += 3)
                    {
                        imageGrid[x, y] = (0.299f * src[2] + 0.587f * src[1] + 0.114f * src[0]);
                        imageGridRed[x, y] = (int)src[2];
                        imageGridGreen[x, y] = (int)src[1];
                        imageGridBlue[x, y] = (int)src[0];
                    }
                    src += offset;
                }
            }
            srcImg.UnlockBits(srcData);
            int[] colorRed = new int[stepY * stepX];
            int[] colorGreen = new int[stepY * stepX];
            int[] colorBlue = new int[stepY * stepX];

            int[] colorRedTemp = new int[stepY * stepX];
            int[] colorGreenTemp = new int[stepY * stepX];
            int[] colorBlueTemp = new int[stepY * stepX];

            int meanRed, meanGreen, meanBlue;
            int t = -1;
            int tempSum = 0;
            double max = 0;
            int temoMaxX = stepX * (int)Math.Sqrt(numberOfBlocks);
            int temoMaxY = stepY * (int)Math.Sqrt(numberOfBlocks);
            for (int y = 0; y < temoMaxY; y += stepY)
            {
                for (int x = 0; x < temoMaxX; x += stepX)
                {
                    meanRed = 0;
                    meanGreen = 0;
                    meanBlue = 0;
                    pixelsNeighborhood.Area1 = 0;
                    pixelsNeighborhood.Area2 = 0;
                    pixelsNeighborhood.Area3 = 0;
                    pixelsNeighborhood.Area4 = 0;
                    edges[0] = -1;
                    edges[1] = -1;
                    edges[2] = -1;
                    edges[3] = -1;
                    edges[4] = -1;
                    edges[5] = -1;
                    for (int i = 0; i < 2; i++)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            pixelCount[i, j] = 0;
                        }
                    }
                    tempSum = 0;
                    for (int i = y; i < y + stepY; i++)
                    {
                        for (int j = x; j < x + stepX; j++)
                        {
                            // Color Information
                            colorRed[tempSum] = imageGridRed[j, i];
                            colorGreen[tempSum] = imageGridGreen[j, i];
                            colorBlue[tempSum] = imageGridBlue[j, i];
                            colorRedTemp[tempSum] = imageGridRed[j, i];
                            colorGreenTemp[tempSum] = imageGridGreen[j, i];
                            colorBlueTemp[tempSum] = imageGridBlue[j, i];
                            tempSum++;

                            // Texture Information
                            if (j < (x + stepX / 2) && i < (y + stepY / 2)) pixelsNeighborhood.Area1 += (imageGrid[j, i]);
                            if (j >= (x + stepX / 2) && i < (y + stepY / 2)) pixelsNeighborhood.Area2 += (imageGrid[j, i]);
                            if (j < (x + stepX / 2) && i >= (y + stepY / 2)) pixelsNeighborhood.Area3 += (imageGrid[j, i]);
                            if (j >= (x + stepX / 2) && i >= (y + stepY / 2)) pixelsNeighborhood.Area4 += (imageGrid[j, i]);
                        }
                    }
                    pixelsNeighborhood.Area1 = (int)(pixelsNeighborhood.Area1 * (4.0 / (stepX * stepY)));
                    pixelsNeighborhood.Area2 = (int)(pixelsNeighborhood.Area2 * (4.0 / (stepX * stepY)));
                    pixelsNeighborhood.Area3 = (int)(pixelsNeighborhood.Area3 * (4.0 / (stepX * stepY)));
                    pixelsNeighborhood.Area4 = (int)(pixelsNeighborhood.Area4 * (4.0 / (stepX * stepY)));

                    maskValues.Mask1 = Math.Abs(pixelsNeighborhood.Area1 * 2 + pixelsNeighborhood.Area2 * -2 + pixelsNeighborhood.Area3 * -2 + pixelsNeighborhood.Area4 * 2);
                    maskValues.Mask2 = Math.Abs(pixelsNeighborhood.Area1 * 1 + pixelsNeighborhood.Area2 * 1 + pixelsNeighborhood.Area3 * -1 + pixelsNeighborhood.Area4 * -1);
                    maskValues.Mask3 = Math.Abs(pixelsNeighborhood.Area1 * 1 + pixelsNeighborhood.Area2 * -1 + pixelsNeighborhood.Area3 * 1 + pixelsNeighborhood.Area4 * -1);
                    maskValues.Mask4 = Math.Abs(pixelsNeighborhood.Area1 * Math.Sqrt(2) + pixelsNeighborhood.Area2 * 0 + pixelsNeighborhood.Area3 * 0 + pixelsNeighborhood.Area4 * -Math.Sqrt(2));
                    maskValues.Mask5 = Math.Abs(pixelsNeighborhood.Area1 * 0 + pixelsNeighborhood.Area2 * Math.Sqrt(2) + pixelsNeighborhood.Area3 * -Math.Sqrt(2) + pixelsNeighborhood.Area4 * 0);

                    max = Math.Max(maskValues.Mask1, Math.Max(maskValues.Mask2, Math.Max(maskValues.Mask3, Math.Max(maskValues.Mask4, maskValues.Mask5))));

                    maskValues.Mask1 = maskValues.Mask1 / max;
                    maskValues.Mask2 = maskValues.Mask2 / max;
                    maskValues.Mask3 = maskValues.Mask3 / max;
                    maskValues.Mask4 = maskValues.Mask4 / max;
                    maskValues.Mask5 = maskValues.Mask5 / max;
                    t = -1;
                    if (max < T0)
                    {
                        edges[0] = 0;
                        t = 0;
                    }
                    else
                    {
                        t = -1;

                        if (maskValues.Mask1 > T1)
                        {
                            t++;
                            edges[t] = 1;
                        }
                        if (maskValues.Mask2 > T2)
                        {
                            t++;
                            edges[t] = 2;
                        }
                        if (maskValues.Mask3 > T2)
                        {
                            t++;
                            edges[t] = 3;
                        }
                        if (maskValues.Mask4 > T3)
                        {
                            t++;
                            edges[t] = 4;
                        }
                        if (maskValues.Mask5 > T3)
                        {
                            t++;
                            edges[t] = 5;
                        }
                    }

                    for (int i = 0; i < (stepY * stepX); i++)
                    {
                        meanRed += colorRed[i];
                        meanGreen += colorGreen[i];
                        meanBlue += colorBlue[i];
                    }

                    meanRed = Convert.ToInt32(meanRed / (stepY * stepX));
                    meanGreen = Convert.ToInt32(meanGreen / (stepY * stepX));
                    meanBlue = Convert.ToInt32(meanBlue / (stepY * stepX));

                    hsv = hsvConverter.ApplyFilter(meanRed, meanGreen, meanBlue);
                    if (Compact == false)
                    {
                        fuzzy10BinResultTable = fuzzy10.ApplyFilter(hsv[0], hsv[1], hsv[2], 2);
                        fuzzy24BinResultTable = fuzzy24.ApplyFilter(hsv[0], hsv[1], hsv[2], fuzzy10BinResultTable, 2);
                        for (int i = 0; i <= t; i++)
                        {
                            for (int j = 0; j < 24; j++)
                            {
                                if (fuzzy24BinResultTable[j] > 0) cedd[24 * edges[i] + j] += fuzzy24BinResultTable[j];
                            }
                        }
                    }
                    else
                    {
                        fuzzy10BinResultTable = fuzzy10.ApplyFilter(hsv[0], hsv[1], hsv[2], 2);
                        for (int i = 0; i <= t; i++)
                        {
                            for (int j = 0; j < 10; j++)
                            {
                                if (fuzzy10BinResultTable[j] > 0) cedd[10 * edges[i] + j] += fuzzy10BinResultTable[j];
                            }
                        }
                    }
                }
            }
            double sum = 0;
            for (int i = 0; i < 144; i++)
            {
                sum += cedd[i];
            }
            for (int i = 0; i < 144; i++)
            {
                cedd[i] = cedd[i] / sum;
            }
            CeddQuant quantization = new CeddQuant();
            cedd = quantization.Apply(cedd);
            return (cedd);
        }

        //Compare CEDD Discriptor using TanimotoClassifier algorimthm
        public static double Compare(double[] table1, double[] table2)
        {
            double result = 0;
            double temp1 = 0;
            double temp2 = 0;
            double tempCount1 = 0, tempCount2 = 0, tempCount3 = 0;
            for (int i = 0; i < table1.Length; i++)
            {
                temp1 += table1[i];
                temp2 += table2[i];
            }

            if (temp1 == 0 || temp2 == 0) result = 100;
            if (temp1 == 0 && temp2 == 0) result = 0;

            if (temp1 > 0 && temp2 > 0)
            {
                for (int i = 0; i < table1.Length; i++)
                {
                    tempCount1 += (table1[i] / temp1) * (table2[i] / temp2);
                    tempCount2 += (table2[i] / temp2) * (table2[i] / temp2);
                    tempCount3 += (table1[i] / temp1) * (table1[i] / temp1);
                }
                result = (100 - 100 * (tempCount1 / (tempCount2 + tempCount3 - tempCount1))); //Tanimoto
            }
            return (result);
        }
    }
}
