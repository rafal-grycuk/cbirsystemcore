﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CbirSystem.Base.Model.Models;
using CbirSystem.ConsoleEndpoint.Extensions;
using CbirSystem.ConsoleEndpoint.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Slack;
using Newtonsoft.Json;

namespace CbirSystem.ConsoleEndpoint
{
    class Program
    {
        private static ILogger _logger;
        private static string[] _extensions = new[] { ".jpg", ".png", ".bmp" };
        private static IDictionary<ImageSet, string> _urls = new Dictionary<ImageSet, string>()
        {
            {ImageSet.Pascal, "http://172.17.206.7/CbirSystemPascal"},
            {ImageSet.Corel, "http://172.17.206.7/CbirSystemCorel"},
            {ImageSet.MicrosoftUnanotated, "http://172.17.206.7/CbirSystemMSU"}
        };


        static void Main()
        {
            Console.WriteLine("Choose your dataset");
            Console.WriteLine("1. Pascal");
            Console.WriteLine("2. Corel");
            Console.WriteLine("3. Microsoft Unanotated");
            var choiseDatasetStr = Console.ReadLine();
            ImageSet imageSet = 0;
            if (int.TryParse(choiseDatasetStr, out int choiseDataset))
            {
                switch (choiseDataset)
                {
                    case 1:
                    {
                        imageSet = ImageSet.Pascal;
                        break;
                    }
                    case 2:
                    {
                        imageSet = ImageSet.Corel;
                        break;
                    }
                    case 3:
                    {
                        imageSet = ImageSet.MicrosoftUnanotated;
                        break;
                    }
                }

                string datasetUrl = _urls[imageSet];
                HttpClient httpClient = new HttpClient();
                var requestContent = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Login", "Admin"),
                    new KeyValuePair<string, string>("Password", "Admin1234")
                });
                var result = httpClient.PostAsync(datasetUrl + "/Account/Login", requestContent).Result;
                if (result.IsSuccessStatusCode)
                {
                    var resultStr = result.Content.ReadAsStringAsync().Result;
                    var token = JsonConvert.DeserializeObject<TokenResponse>(resultStr);

                    //setup DI
                    var serviceProvider = new ServiceCollection()
                        .AddLogging()
                        .BuildServiceProvider();

                    ILoggerFactory loggerFactory = serviceProvider.GetService<ILoggerFactory>();
                    loggerFactory.AddConsole();
                    loggerFactory.AddFile("Logs/CbirSystem-{Date}.txt");
                    var configuration = new SlackConfiguration()
                    {
                        WebhookUrl =
                            new Uri("https://hooks.slack.com/services/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"),
                        MinLevel = LogLevel.Warning
                    };
                    loggerFactory.AddSlack(configuration, "Endpoint", "env");
                    _logger = loggerFactory.CreateLogger<Program>();

                    Console.WriteLine("1. Upload images.");
                    Console.WriteLine("2. Simulate multiquery");
                    var choiseStr = Console.ReadLine();
                    if (int.TryParse(choiseStr, out int choise))
                    {
                        switch (choise)
                        {
                            case 1:
                            {
                                AddAllImages(datasetUrl + "/CeddIndexer/InsertImage", token.AccessToken, imageSet).Wait();
                                Console.WriteLine("Done !");
                                _logger.LogInformation("All Images Added.");
                                Console.ReadKey();
                                break;
                            }
                            case 2:
                            {

                                SimulateMultiQuery(datasetUrl + "/Simulator/Simulate", token.AccessToken, imageSet).Wait();
                                Console.WriteLine("Done !");
                                _logger.LogInformation("Simulation DONE.");
                                break;
                            }
                        }
                    }
                }
            }
        }
        private static async Task<HttpResponseMessage> UploadImage(string url, string token, byte[] imageBytes, FileInfo fileInfo, string tag)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
            var requestContent = new MultipartFormDataContent();
            var imageContent = new ByteArrayContent(imageBytes);
            requestContent.Add(imageContent, tag, fileInfo.FullName);
            return await httpClient.PostAsync(url, requestContent);
        }
        private static async Task AddAllImages(string url, string token, ImageSet imageSet)
        {
            string path = EnumExtensions.GetPath(imageSet);
            string trainPath = $@"{path}\train";
            int totalImages = 0;
            if (Directory.Exists(trainPath))
            {
                var folders = Directory.GetDirectories(trainPath).ToList();
                foreach (var folder in folders)
                {
                    DirectoryInfo di = new DirectoryInfo(folder);
                    var files = di.GetFiles()
                        .Where(f => _extensions.Contains(f.Extension.ToLower()))
                        .ToArray();
                    foreach (var file in files)
                    {
                        byte[] imageBytes = File.ReadAllBytes(file.FullName);
                        var result = await UploadImage(url, token, imageBytes, file, di.Name);
                        if (result.IsSuccessStatusCode)
                        {
                            Console.WriteLine(file.FullName);
                            ++totalImages;
                        }
                        else
                        {
                            _logger.LogError(result.ReasonPhrase + $" | FileName: {file.FullName}, Tag: {di.Name}");
                        }
                    }
                }
                Console.WriteLine(totalImages);
            }
        }
        private static async Task SimulateMultiQuery(string url, string token, ImageSet imageSet)
        {
            string path = EnumExtensions.GetPath(imageSet);
            string evalPath = $@"{path}\eval";
            if (Directory.Exists(evalPath))
            {
                var folders = Directory.GetDirectories(evalPath).ToList();
                var requestContent = new MultipartFormDataContent();
                foreach (var folder in folders)
                {
                    DirectoryInfo di = new DirectoryInfo(folder);
                    var files = di.GetFiles()
                        .Where(f => _extensions.Contains(f.Extension.ToLower()))
                        .ToArray();
                    foreach (var file in files)
                    {
                        byte[] imageBytes = File.ReadAllBytes(file.FullName);
                        var imageContent = new ByteArrayContent(imageBytes);
                        requestContent.Add(imageContent, di.Name, file.FullName);
                    }

                }
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
                httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                httpClient.Timeout = new TimeSpan(10, 0, 0, 0, 0);
                Console.WriteLine("Request sent. Processing......");
                var response = await httpClient.PostAsync(url, requestContent);
                if (response.IsSuccessStatusCode)
                {
                    var responseStr = response.Content.ReadAsStringAsync().Result;
                    var simulationResults = JsonConvert.DeserializeObject<SimulationResults>(responseStr);
                    Console.WriteLine(simulationResults);
                }
                else
                    Console.WriteLine("ERROR: " + response.ReasonPhrase);
                Console.ReadKey();
            }
        }
    }
}
