﻿using System.Threading.Tasks;

namespace CbirSystem.Base.Logic
{
    public abstract class AbstractFeatureIndexerService<TFeatureType>
    {
        protected readonly IFeatureExtractorService<TFeatureType> _extractor;
        public AbstractFeatureIndexerService(IFeatureExtractorService<TFeatureType> extractor)
        {
            _extractor = extractor;
        }

        public abstract Task CreateIndex(params object[] param);
        public abstract void DeleteIndex();
    }
}
