﻿using System;
using System.Collections.Generic;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    public class BkTree<T> where T : BkTreeNode
    {
        private readonly Dictionary<T, Int32> _matches;

        public T Root { get; set; }

        public void Clear()
        {
            Root.Children.Clear();
        }

        public int Count
        {
            get
            {
                if (Root?.Children == null)
                    return 0;
                return Root.Children.Count;
            }
        }

        public BkTree()
        {
            _matches = new Dictionary<T, Int32>();
        }

        public void Add(T node)
        {
            if (Root != null)
            {
                Root.Add(node);
            }
            else
            {
                Root = node;
            }
        }

        /**
         * This method will find all the close matching Nodes within
         * a certain threshold.  For instance, to search for similar
         * strings, threshold set to 1 will return all the strings that
         * are off by 1 edit distance.
         * @param searchNode
         * @param threshold
         * @return
         */
        public Dictionary<T, Int32> Query(BkTreeNode searchNode, Int32 threshold)
        {
            Dictionary<BkTreeNode, Int32> matches = new Dictionary<BkTreeNode, Int32>();
            Root.Query(searchNode, threshold, matches);
            return CopyMatches(matches);
        }

        /**
         * Attempts to find the closest match to the search node.
         * @param node 
         * @return The edit distance of the best match
         */
        public Int32 FindBestDistance(BkTreeNode node)
        {
            return Root.FindBestMatch(node, Int32.MaxValue, out BkTreeNode bestNode);
        }

        /**
         * Attempts to find the closest match to the search node.
         * @param node
         * @return A match that is within the best edit distance of the search node.
         */
        public T FindBestNode(BkTreeNode node)
        {
            Root.FindBestMatch(node, Int32.MaxValue, out BkTreeNode bestNode);
            return (T)bestNode;
        }

        /**
         * Attempts to find the closest match to the search node.
         * @param node
         * @return A match that is within the best edit distance of the search node.
         */
        public Dictionary<T, Int32> FindBestNodeWithDistance(BkTreeNode node)
        {
            Int32 distance = Root.FindBestMatch(node, Int32.MaxValue, out BkTreeNode bestNode);
            _matches.Clear();
            _matches.Add((T)bestNode, distance);
            return _matches;
        }

        private Dictionary<T, Int32> CopyMatches(Dictionary<BkTreeNode, Int32> source)
        {
            _matches.Clear();
            foreach (KeyValuePair<BkTreeNode, Int32> pair in source)
            {
                _matches.Add((T)pair.Key, pair.Value);
            }
            return _matches;
        }
    }
}
