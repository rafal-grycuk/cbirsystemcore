﻿using System.ComponentModel.DataAnnotations;

namespace CbirSystem.ViewModels
{
    public class LoginUserVm
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
