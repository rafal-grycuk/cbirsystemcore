﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using CbirSystem.VisualizationEndpoint.Extensions;
using CbirSystem.VisualizationEndpoint.Models;
using log4net;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CbirSystem.VisualizationEndpoint
{
    public partial class Simulator : Form
    {
        private static ImageSet _imageSet = ImageSet.Corel;
        private static string[] _extensions = new[] { ".jpg", ".png", ".bmp" };
        private string _token = string.Empty;
        private const string _url = "http://172.17.206.7/CbirSystemCorel";
        private SimulationResults _results;
        private SimulationResults _trimedResults;
        private static readonly ILog _log = LogManager.GetLogger("AllEvents");
        private static readonly ILog _slackLog = LogManager.GetLogger("Slack");
        public int LinesBorder { get; set; }
        public bool EnabledDots { get; set; }

        public Simulator()
        {
            this.LinesBorder = 4;
            this.EnabledDots = true;
            InitializeComponent();
            HttpClient httpClient = new HttpClient();
            var requestContent = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Login", "Admin"),
                    new KeyValuePair<string, string>("Password", "Admin1234")
                });
            var result = httpClient.PostAsync(_url + "/Account/Login", requestContent).Result;
            if (result.IsSuccessStatusCode)
            {
                var resultStr = result.Content.ReadAsStringAsync().Result;
                _token = $"Bearer {JsonConvert.DeserializeObject<TokenResponse>(resultStr).AccessToken}";
            }
        }
        private static async Task<SimulationResults> SimulateMultiQuery(string url, string token)
        {
            string path = EnumExtensions.GetPath(_imageSet);
            string evalPath = $@"{path}\eval";
            if (Directory.Exists(evalPath))
            {
                var folders = Directory.GetDirectories(evalPath).ToList();
                var requestContent = new MultipartFormDataContent();
                foreach (var folder in folders)
                {
                    DirectoryInfo di = new DirectoryInfo(folder);
                    var files = di.GetFiles()
                        .Where(f => _extensions.Contains(f.Extension.ToLower()))
                        .ToArray();
                    foreach (var file in files)
                    {
                        byte[] imageBytes = File.ReadAllBytes(file.FullName);
                        var imageContent = new ByteArrayContent(imageBytes);
                        requestContent.Add(imageContent, di.Name, file.FullName);
                    }

                }
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
                httpClient.DefaultRequestHeaders.Add("Authorization", token);
                httpClient.Timeout = new TimeSpan(10, 0, 0, 0, 0);
                var response = await httpClient.PostAsync(url, requestContent);
                if (response.IsSuccessStatusCode)
                {
                    var responseStr = response.Content.ReadAsStringAsync().Result;
                    SimulationResults simulationResults = JsonConvert.DeserializeObject<SimulationResults>(responseStr);
                    return simulationResults;
                }
                else
                {
                    throw new Exception("Unexpected Error.");
                }
            }
            else throw new Exception("Folder is empty");
        }

        private async void Simulator_Load(object sender, EventArgs e)
        {
            try
            {
                Random rand = new Random(Guid.NewGuid().GetHashCode());
                int? queryEachClass = _imageSet == ImageSet.MicrosoftUnanotated ? 50 : new int?();
                _results = await SimulateMultiQuery(_url + "/Simulator/Simulate", _token);
                this._trimedResults = new SimulationResults()
                {
                    RetrievalFactors = queryEachClass.HasValue
                        ? _results.RetrievalFactors.Skip(rand.Next(0, 150)).Take(queryEachClass.Value).ToList()
                        : _results.RetrievalFactors.Take(50).ToList(),
                    AvgPrecision = _results.AvgPrecision,
                    AvgRecall = _results.AvgRecall
                };
                foreach (var result in this._trimedResults.RetrievalFactors)
                {
                    textBoxExcel.Text += result.ToExcel() + "\r\n";
                    textBoxOutput.Text += result + "\r\n";
                }
                textBoxOutput.Text += "++++++++++++++++++ Avarage Precision (MAP)++++++++\r\n";

                _log.Info("Avg Precision: " + _results.AvgPrecision);
                textBoxOutput.Text += _results.AvgPrecision;
                textBoxOutput.Text += "++++++++++++++++++ Avarage Recall (MAP)++++++++\r\n";
                textBoxOutput.Text += _results.AvgRecall;
                ReLoadChart();
                DateTime end = DateTime.Now;
            }
            catch (Exception ex)
            {
                _slackLog.Error(ex.Message, ex);
            }
        }

        public void ReLoadChart()
        {
            chartFactors.Series.Clear();
            chartClassFactors.Series.Clear();
            chartPrecRecall.Series.Clear();

            Series rai = new Series("RAI")
            {
                ChartType = SeriesChartType.Spline,
                BorderWidth = LinesBorder,
                BorderDashStyle = ChartDashStyle.Solid,
                MarkerSize = LinesBorder,
                Color = Color.Blue
            };

            Series anr = new Series("ANR")
            {
                ChartType = SeriesChartType.Spline,
                BorderWidth = LinesBorder,
                BorderDashStyle = ChartDashStyle.DashDot,
                MarkerSize = LinesBorder,
                Color = Color.Gold
            };

            Series iri = new Series("IRI")
            {
                ChartType = SeriesChartType.Spline,
                BorderWidth = LinesBorder,
                BorderDashStyle = ChartDashStyle.DashDotDot,
                MarkerSize = LinesBorder,
                Color = Color.Red
            };


            Series ri = new Series("RI")
            {
                ChartType = SeriesChartType.Spline,
                BorderWidth = LinesBorder,
                BorderDashStyle = ChartDashStyle.DashDot,
                Color = Color.Green,
                MarkerSize = LinesBorder
            };

            Series precisionRecall = new Series("Precision(Recall)")
            {
                ChartType = SeriesChartType.Spline,
                BorderWidth = LinesBorder,
                BorderDashStyle = ChartDashStyle.Solid,
                Color = Color.Red,
                MarkerSize = LinesBorder
            };

            //Series recall = new Series("Recall");
            //recall.ChartType = SeriesChartType.Spline;
            //recall.BorderWidth = LinesBorder;
            //recall.Color = Color.Black;
            //recall.BorderDashStyle = ChartDashStyle.Solid;
            //recall.MarkerSize = LinesBorder;

            foreach (var result in this._trimedResults.RetrievalFactors.OrderByDescending(x => x.Precision).ThenByDescending(x => x.Recall))
            {
                rai.Points.Add(new DataPoint() { YValues = new double[] { (double)result.CorrectRetrivedImages }, MarkerSize = this.EnabledDots ? LinesBorder * 2 : 0, MarkerStyle = MarkerStyle.Circle });
                iri.Points.Add(new DataPoint() { YValues = new double[] { (double)result.InappropriateReturnedImages }, MarkerSize = this.EnabledDots ? LinesBorder * 2 : 0, MarkerStyle = MarkerStyle.Circle });
                anr.Points.Add(new DataPoint() { YValues = new double[] { (double)result.AppropriateNotReturned }, MarkerSize = this.EnabledDots ? LinesBorder * 2 : 0, MarkerStyle = MarkerStyle.Circle });
                ri.Points.Add(new DataPoint() { YValues = new double[] { (double)result.RetrivedImages }, MarkerSize = this.EnabledDots ? LinesBorder * 2 : 0, MarkerStyle = MarkerStyle.Circle });
                precisionRecall.Points.Add(new DataPoint() { YValues = new double[] { (double)result.Precision }, XValue = (double)result.Recall, MarkerSize = this.EnabledDots ? LinesBorder * 2 : 0, MarkerStyle = MarkerStyle.Circle });
                //recall.Points.Add(new DataPoint() { YValues = new double[] { (double)result.Recall }, MarkerSize = this.EnabledDots ? LinesBorder * 2 : 0, MarkerStyle = MarkerStyle.Circle });
            }
            chartFactors.Series.Add(rai);
            chartFactors.Series.Add(anr);
            chartFactors.Series.Add(iri);
            chartFactors.Series.Add(ri);

            chartPrecRecall.Series.Add(precisionRecall);
            //chartPrecRecall.Series.Add(recall);

            var groupedRai = this._results.RetrievalFactors.GroupBy(r => r.QueryImageTag)
                    .Select(g => new { Class = g.Key, AVG = g.Average(z => z.Precision) }).ToList();
            Series groupedRaiSeries = new Series("AVG(Precision)")
            {
                XValueType = ChartValueType.String
            };
            foreach (var group in groupedRai)
            {
                groupedRaiSeries.Points.AddXY(group.Class, group.AVG);
            }
            groupedRaiSeries.MarkerSize = 1;
            groupedRaiSeries.ChartType = SeriesChartType.Bar;
            groupedRaiSeries.Font = new Font("Microsoft Sans Serif", 16);
            chartClassFactors.Series.Add(groupedRaiSeries);

            chartClassFactors.ChartAreas[0].AxisX.Interval = 1;

            //chartPrecRecall.ChartAreas[0].AxisX.Interval = 5;
            //chartPrecRecall.ChartAreas[0].AxisY.Interval = 5;
            chartPrecRecall.ChartAreas[0].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;

            //double interval = Math.Floor(this._trimedResults.Max(x => x.AppropriateImages) * 0.1);
            //interval = ((int)Math.Round(interval / 10.0)) * 10;
            //chartFactors.ChartAreas[0].AxisX.Interval = 10;
            //chartFactors.ChartAreas[0].AxisY.Interval = interval;

        }

        private void showLatexToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxOutput.Text = "";
            foreach (var result in this._trimedResults.RetrievalFactors)
                textBoxOutput.Text += result.ToLatex().Replace("_", "-") + "\r\n";
            textBoxOutput.Text += @"\hline" + "\r\n";
            textBoxOutput.Text += @"Average Precision &  &  &  &  &  &  " + this._trimedResults.AvgPrecision + " & " + this._trimedResults.AvgRecall + "\\\\" + "\r\n"; ;
            textBoxOutput.Text += @"\hline" + "\r\n";
        }

        private void chartFactors_Click(object sender, EventArgs e)
        {
            if (sender is Chart)
            {
                Chart chartSender = sender as Chart;
                ShowFullscreenChartHandler(chartSender);
            }
        }

        private void chartClassFactors_Click(object sender, EventArgs e)
        {
            if (sender is Chart)
            {
                Chart chartSender = sender as Chart;
                ShowFullscreenChartHandler(chartSender);
            }
        }

        private void ShowFullscreenChartHandler(Chart sender)
        {
            ChartForm chartForm = new ChartForm();
            chartForm.Chart.Series.Clear();
            chartForm.Chart.ChartAreas.Clear();
            foreach (var serie in sender.Series)
            {
                chartForm.Chart.Series.Add(serie);
            }
            foreach (var legend in chartForm.Chart.Legends)
            {
                legend.Font = new Font(legend.Font.FontFamily, 14);
            }
            foreach (var chartArea in sender.ChartAreas)
            {
                chartForm.Chart.ChartAreas.Add(chartArea);
            }

            chartForm.ShowDialog();
        }

        private void biggerLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LinesBorder++;
            ReLoadChart();
        }

        private void smallerLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.LinesBorder > 1)
            {
                LinesBorder--;
                ReLoadChart();
            }

        }

        private void enableDotsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.EnabledDots = true;
            ReLoadChart();
        }

        private void disableDotsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.EnabledDots = false;
            ReLoadChart();
        }

        private void chartPrecRecall_Click(object sender, EventArgs e)
        {
            if (sender is Chart)
            {
                Chart chartSender = sender as Chart;
                ShowFullscreenChartHandler(chartSender);
            }
        }
    }
}
