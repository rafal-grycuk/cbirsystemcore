﻿using System;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    class Fuzzy10Bin
    {
        public struct FuzzyRules
        {
            public int Input1 { get; set; }
            public int Input2 { get; set; }
            public int Input3 { get; set; }
            public int Output { get; set; }
        }
        public bool KeepPreviuesValues;
        protected double[] HueMembershipValues = {
            0, 0, 5, 10,
            5, 10, 35, 50,
            35, 50, 70, 85,
            70, 85, 150, 165,
            150, 165, 195, 205,
            195, 205, 265, 280,
            265, 280, 315, 330,
            315, 330, 360, 360
        };
        protected double[] SaturationMembershipValues = {
            0, 0, 10, 75,
            10, 75, 255, 255
        };

        protected double[] ValueMembershipValues = {
            0, 0, 10, 75,
            10, 75, 180, 220,
            180, 220, 255, 255
        };



        public FuzzyRules[] Fuzzy10BinRules = new FuzzyRules[48];
        public double[] Fuzzy10BinHisto = new double[10];
        public double[] HueActivation = new double[8];
        public double[] SaturationActivation = new double[2];
        public double[] ValueActivation = new double[3];

        public int[,] Fuzzy10BinRulesDefinition = {
            {0, 0, 0, 2},
            {0, 1, 0, 2},
            {0, 0, 2, 0},
            {0, 0, 1, 1},
            {1, 0, 0, 2},
            {1, 1, 0, 2},
            {1, 0, 2, 0},
            {1, 0, 1, 1},
            {2, 0, 0, 2},
            {2, 1, 0, 2},
            {2, 0, 2, 0},
            {2, 0, 1, 1},
            {3, 0, 0, 2},
            {3, 1, 0, 2},
            {3, 0, 2, 0},
            {3, 0, 1, 1},
            {4, 0, 0, 2},
            {4, 1, 0, 2},
            {4, 0, 2, 0},
            {4, 0, 1, 1},
            {5, 0, 0, 2},
            {5, 1, 0, 2},
            {5, 0, 2, 0},
            {5, 0, 1, 1},
            {6, 0, 0, 2},
            {6, 1, 0, 2},
            {6, 0, 2, 0},
            {6, 0, 1, 1},
            {7, 0, 0, 2},
            {7, 1, 0, 2},
            {7, 0, 2, 0},
            {7, 0, 1, 1},
            {0, 1, 1, 3},
            {0, 1, 2, 3},
            {1, 1, 1, 4},
            {1, 1, 2, 4},
            {2, 1, 1, 5},
            {2, 1, 2, 5},
            {3, 1, 1, 6},
            {3, 1, 2, 6},
            {4, 1, 1, 7},
            {4, 1, 2, 7},
            {5, 1, 1, 8},
            {5, 1, 2, 8},
            {6, 1, 1, 9},
            {6, 1, 2, 9},
            {7, 1, 1, 3},
            {7, 1, 2, 3}
        };

        public Fuzzy10Bin(bool keepPreviuesValues)
        {
            for (int R = 0; R < 48; R++)
            {

                Fuzzy10BinRules[R].Input1 = Fuzzy10BinRulesDefinition[R, 0];
                Fuzzy10BinRules[R].Input2 = Fuzzy10BinRulesDefinition[R, 1];
                Fuzzy10BinRules[R].Input3 = Fuzzy10BinRulesDefinition[R, 2];
                Fuzzy10BinRules[R].Output = Fuzzy10BinRulesDefinition[R, 3];
            }
            this.KeepPreviuesValues = keepPreviuesValues;
        }

        private void FindMembershipValueForTriangles(double input, double[] triangles, double[] membershipFunctionToSave)
        {
            int temp = 0;
            for (int i = 0; i <= triangles.Length - 1; i += 4)
            {
                membershipFunctionToSave[temp] = 0;
                if (input >= triangles[i + 1] && input <= +triangles[i + 2])
                {
                    membershipFunctionToSave[temp] = 1;
                }

                if (input >= triangles[i] && input < triangles[i + 1])
                {
                    membershipFunctionToSave[temp] = (input - triangles[i]) / (triangles[i + 1] - triangles[i]);
                }
                
                if (input > triangles[i + 2] && input <= triangles[i + 3])
                {
                    membershipFunctionToSave[temp] =
                        (input - triangles[i + 2]) / (triangles[i + 2] - triangles[i + 3]) + 1;
                }
                temp += 1;
            }
        }

        private void LomDefazzificator(FuzzyRules[] rules, double[] input1, double[] input2, double[] input3, double[] resultTable)
        {
            int ruleActivation = -1;
            double lomMaxOfMin = 0;
            for (int i = 0; i < rules.Length; i++)
            {
                if ((input1[rules[i].Input1] > 0) && (input2[rules[i].Input2] > 0) && (input3[rules[i].Input3] > 0))
                {
                    double min;
                    min = Math.Min(input1[rules[i].Input1], Math.Min(input2[rules[i].Input2], input3[rules[i].Input3]));
                    if (min > lomMaxOfMin)
                    {
                        lomMaxOfMin = min;
                        ruleActivation = rules[i].Output;
                    }
                }
            }
            resultTable[ruleActivation]++;
        }


        private void MultiParticipateEqualDefazzificator(FuzzyRules[] rules, double[] input1, double[] input2, double[] input3, double[] resultTable)
        {
            int ruleActivation = -1;
            for (int i = 0; i < rules.Length; i++)
            {
                if ((input1[rules[i].Input1] > 0) && (input2[rules[i].Input2] > 0) && (input3[rules[i].Input3] > 0))
                {
                    ruleActivation = rules[i].Output;
                    resultTable[ruleActivation]++;
                }
            }
        }
        private void MultiParticipateDefazzificator(FuzzyRules[] rules, double[] input1, double[] input2, double[] input3, double[] resultTable)
        {
            int ruleActivation = -1;
            for (int i = 0; i < rules.Length; i++)
            {
                if ((input1[rules[i].Input1] > 0) && (input2[rules[i].Input2] > 0) && (input3[rules[i].Input3] > 0))
                {
                    ruleActivation = rules[i].Output;
                    double min = Math.Min(input1[rules[i].Input1], Math.Min(input2[rules[i].Input2], input3[rules[i].Input3]));
                    resultTable[ruleActivation] += min;
                }
            }
        }

        public double[] ApplyFilter(double hue, double saturation, double value, int method)
        {
            if (KeepPreviuesValues == false)
            {
                for (int i = 0; i < 10; i++)
                {
                    Fuzzy10BinHisto[i] = 0;
                }
            }
            FindMembershipValueForTriangles(hue, HueMembershipValues, HueActivation);
            FindMembershipValueForTriangles(saturation, SaturationMembershipValues, SaturationActivation);
            FindMembershipValueForTriangles(value, ValueMembershipValues, ValueActivation);
            if (method == 0)
                LomDefazzificator(Fuzzy10BinRules, HueActivation, SaturationActivation, ValueActivation,
                    Fuzzy10BinHisto);
            if (method == 1)
                MultiParticipateEqualDefazzificator(Fuzzy10BinRules, HueActivation, SaturationActivation,
                    ValueActivation, Fuzzy10BinHisto);
            if (method == 2)
                MultiParticipateDefazzificator(Fuzzy10BinRules, HueActivation, SaturationActivation, ValueActivation,
                    Fuzzy10BinHisto);
            return (Fuzzy10BinHisto);
        }
    }
}