﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CbirSystem.Base.Model.Models
{
    public class Feature<TFeatureType>
    {
        private static int _count = 1;

        public Feature()
        {
            this.Id = _count;
            _count++;
        }

        public TFeatureType Data { get; set; }
        public int? GroupId { get; set; }
        public int Id { get; set; }
        public int ImageId { get; set; }
        public Indexer IndexerType { get; set; }
       
        
    }
}
