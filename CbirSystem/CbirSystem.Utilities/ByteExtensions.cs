﻿using System;
using System.Linq;

namespace CbirSystem.Utilities
{
    public static class ByteExtensions
    {
        public static byte[] ToByteArray(double[] doubleArray)
        {
            return doubleArray.SelectMany(value => BitConverter.GetBytes(value)).ToArray();
        }

        public static double[] ToDoubleArray(byte[] byteArray)
        {
            return Enumerable.Range(0, byteArray.Length / sizeof(double))
                .Select(offset => BitConverter.ToDouble(byteArray, offset * sizeof(double)))
                .ToArray();
        }
    }
}
