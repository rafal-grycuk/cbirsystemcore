﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CbirSystem.Api.Controllers
{
    public abstract class IndexerController<TFeatureType> : BaseApiController
    {
        protected readonly AbstractExecutorService<TFeatureType> _executor;
        public IndexerController(IUnitOfWork uow, ILoggerFactory loggerFactory, AbstractExecutorService<TFeatureType> executor) : base(uow, loggerFactory)
        {
            _executor = executor;
        }

        [HttpPost("[Action]")]
        public virtual async Task<IActionResult> GetFeatures(int imageId)
        {
            var image = _uow.Repository<Image>().Get(imageId, false);
            var result = await _executor.FeatureExtractor.ExtractFeatures(image).ContinueWith(x => Ok(x.Result));
            return Ok(result?.Value);
        }

        [HttpPost("[Action]")]
        public virtual async Task<IActionResult> ExtractAllFeatures()
        {
            var images = _uow.Repository<Image>().GetRange(enableTracking: false).ToList();
            var result = await _executor.FeatureExtractor.ExtractAllFeatures(images).ContinueWith(x => Ok(x.Result));
            return Ok(result);
        }

        [HttpPost("[Action]")]
        public virtual async Task<IActionResult> CreateIndex()
        {
            DateTime start = DateTime.Now;
            string controllerName = ControllerContext.RouteData.Values["controller"].ToString();
            if (controllerName.Contains(Indexer.Surf.ToString()) && _uow.Repository<FeatureEntity>().Count(x => x.IndexerType == Indexer.Surf) == 0
                ||
                controllerName.Contains(Indexer.Cedd.ToString()) && _uow.Repository<FeatureEntity>().Count(x => x.IndexerType == Indexer.Cedd) == 0
                )
            {
                await ExtractAllFeatures();
            }
            await _executor.CreateIndex();
            DateTime end = DateTime.Now;
            TimeSpan computationTime = (end - start);
            var info = $"Index created. The computation time is equal: {computationTime}";
            _logger.LogWarning(info);
            return Ok(info);
        }

        [HttpPost("[Action]")]
        public virtual IActionResult DeleteIndex()
        {
            _executor.DeleteIndex();
            return Ok("Index deleted");
        }

        [HttpPost("[Action]")]
        public virtual IActionResult InsertImage()
        {
            try
            {
                var files = Request?.Form?.Files;
                if (files?.Count > 0)
                {
                    foreach (var file in files)
                    {
                        Image image = new Image()
                        {
                            Extension = file.FileName.Substring(file.FileName.IndexOf('.'),
                                file.FileName.Length - file.FileName.IndexOf('.')),
                            ImageName = System.IO.Path.GetFileName(file.FileName),
                            RawImage = Image.ImageToByte((System.Drawing.Bitmap)System.Drawing.Image.FromStream(file.OpenReadStream())),
                            Tag = file.Name
                        };
                        _uow.Repository<Image>().Add(image);
                        _uow.Save();
                    }
                    return Ok("Image successfully added.");
                }
                else
                    return StatusCode(500, "No files selected.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return StatusCode(500, "Error occured");
            }
        }

        [HttpPost("[Action]")]
        public virtual IActionResult ExecuteQuery(double? distance = null)
        {
            try
            {
                var files = Request?.Form?.Files;
                if (files?.Count > 0)
                {
                    foreach (var file in files)
                    {
                        var image = new Image()
                        {
                            Extension = file.FileName.Substring(file.FileName.IndexOf('.'),
                                file.FileName.Length - file.FileName.IndexOf('.')),
                            ImageName = System.IO.Path.GetFileName(file.FileName),
                            RawImage = Image.ImageToByte((System.Drawing.Bitmap)System.Drawing.Image.FromStream(file.OpenReadStream())),
                            Tag = file.Name
                        };
                        var results = _executor.ExecuteQuery(image);
                        return Ok(results);
                    }
                    return Ok("Image successfully added.");
                }
                else
                    return StatusCode(500, "No files selected.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return StatusCode(500, "Error occured");
            }
        }

    }
}
