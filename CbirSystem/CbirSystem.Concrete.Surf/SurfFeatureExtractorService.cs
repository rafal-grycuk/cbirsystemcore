﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accord.Imaging;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using CbirSystem.Utilities;
using DataAccessLayer.Core.Interfaces.UoW;

namespace CbirSystem.Concrete.Surf
{
    public class SurfFeatureExtractorService : IFeatureExtractorService<SpeededUpRobustFeaturePoint>
    {
        private readonly SpeededUpRobustFeaturesDetector _detector;
        private readonly IUnitOfWork _uow;
        public SurfFeatureExtractorService(SpeededUpRobustFeaturesDetector detector, IUnitOfWork uow)
        {
            _detector = detector;
            _uow = uow;
        }

        public async Task<IList<Feature<SpeededUpRobustFeaturePoint>>> ExtractFeatures(Base.Model.Models.Image image)
        {
            return await Task.Run<IList<Feature<SpeededUpRobustFeaturePoint>>>(() =>
           {
               var features = _detector.ProcessImage(image).Select(f => new Feature<SpeededUpRobustFeaturePoint>()
               {
                   ImageId = image.Id,
                   Data = f,
                   IndexerType = Indexer.Surf

               }).ToList();
               return features;
           });

        }

        public async Task<IList<Feature<SpeededUpRobustFeaturePoint>>> ExtractAllFeatures(IList<Base.Model.Models.Image> images)
        {
            List<Feature<SpeededUpRobustFeaturePoint>> features = new List<Feature<SpeededUpRobustFeaturePoint>>();
            foreach (var image in images)
                await ExtractFeatures(image).ContinueWith(x =>
                {
                    if (x.IsCompletedSuccessfully)
                        features.AddRange(x.Result);
                });
            var featureMatrixEntities = ToFeatureMatrixEntity(features);
            _uow.Repository<FeatureEntity>().AddRange(featureMatrixEntities);
            _uow.Save();
            return features;
        }

        public static Array GetFeatureMatrix(IList<Feature<SpeededUpRobustFeaturePoint>> features)
        {
            var matrix = new double[features.Count][];
            for (int i = 0; i < features.Count; i++)
            {
                matrix[i] = new double[features[i].Data.Descriptor.Length];
                for (int j = 0; j < features[i].Data.Descriptor.Length; j++)
                {
                    matrix[i][j] = features[i].Data.Descriptor[j];
                }
            }
            return matrix;
        }

        public static IList<FeatureEntity> ToFeatureMatrixEntity(IList<Feature<SpeededUpRobustFeaturePoint>> features)
        {
            IList<FeatureEntity> featureMatrixEntity = features.Select(fr => new FeatureEntity
            {
                FeatureVector = ByteExtensions.ToByteArray(fr.Data.Descriptor),
                IndexerType = fr.IndexerType,
                ImageId = fr.ImageId
            }).ToList();
            return featureMatrixEntity;
        }

        public static IList<Feature<SpeededUpRobustFeaturePoint>> ToFeatureMatrix(ICollection<FeatureEntity> featureMatrixEntity)
        {
            IList<Feature<SpeededUpRobustFeaturePoint>> featureMatrix = featureMatrixEntity.Select(fr => new Feature<SpeededUpRobustFeaturePoint>()
            {
                Data = new SpeededUpRobustFeaturePoint(0, 0, 0, 0, 0, 0, ByteExtensions.ToDoubleArray(fr.FeatureVector)),
                IndexerType = fr.IndexerType
            }).ToList();
            return featureMatrix;
        }
    }
}
