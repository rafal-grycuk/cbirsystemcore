﻿using System;
using System.Collections.Generic;
using System.Linq;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using CbirSystem.Concrete.Cedd.CEDD;
using DataAccessLayer.Core.Interfaces.UoW;

namespace CbirSystem.Concrete.Cedd
{
    public class CeddExecutorService : AbstractExecutorService<double[]>
    {
        private readonly IUnitOfWork _uow;
        public CeddExecutorService(AbstractFeatureIndexerService<double[]> indexer, IFeatureExtractorService<double[]> extractor, IUnitOfWork uow)
            : base(indexer, extractor)
        {
            _uow = uow;
        }

        public override IList<Image> ExecuteQuery(Image query, params object[] parameters)
        {
            if (_featureIndexer is CeddFeatureIndexerService)
            {
                CeddFeatureIndexerService indexer =  _featureIndexer as CeddFeatureIndexerService;
                if (indexer.CeddTree.Count == 0)
                    CreateIndex().Wait();

                int goodMatchDistance = 35;
                if (parameters != null && parameters.Any() && parameters[0] != null &&
                    int.TryParse(parameters[0].ToString(), out goodMatchDistance)){}
                double[] queryCeddDiscriptor;
                CEDD.Cedd cedd = new CEDD.Cedd();
                using (System.Drawing.Bitmap bmp = query)
                {
                    queryCeddDiscriptor = cedd.Apply(bmp);
                }
                CeddTreeNode queryNode = new CeddTreeNode
                {
                    Id = 0,
                    CeddDiscriptor = queryCeddDiscriptor
                };
                Dictionary<CeddTreeNode, Int32> results = indexer.CeddTree.Query(queryNode, goodMatchDistance);
                var images = _uow.Repository<Image>()
                    .GetRange(img => results.Any(res => res.Key.Id.Equals(img.Id)))
                    .Join(results, imgs => imgs.Id, res => res.Key.Id, (img, res) => new Image()
                    {
                        Extension = img.Extension,
                        Id = img.Id,
                        ImageName = img.ImageName,
                        RawImage = img.RawImage,
                        Tag = img.Tag,
                        Distance = res.Value
                    })
                    .OrderBy(x => x.Distance);
                return images.ToList();
            }
            else
                throw new Exception("The feature indexer is not CeddIndexer.");
        }
    }
}
