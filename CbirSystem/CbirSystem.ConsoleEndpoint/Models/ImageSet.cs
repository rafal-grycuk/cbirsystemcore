﻿using CbirSystem.ConsoleEndpoint.Filters;

namespace CbirSystem.ConsoleEndpoint.Models
{
    public enum ImageSet
    {
        [PathInfo(@"ImageDatasets\Pascal")]
        Pascal,

        [PathInfo(@"ImageDatasets\CorelDb")]
        Corel,

        [PathInfo(@"ImageDatasets\MicrosoftUnanotated")]
        MicrosoftUnanotated
    }
}
