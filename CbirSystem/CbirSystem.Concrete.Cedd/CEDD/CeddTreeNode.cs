﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    public class CeddTreeNode : BkTreeNode
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public double Distance { get; set; }
        public double[] CeddDiscriptor { get; set; }
        protected override Int32 CalculateDistance(BkTreeNode node)
        {
            MemorySize.Hits++;
            double[] ceddDiscriptor1 = CeddDiscriptor;
            double[] ceddDiscriptor2 = ((CeddTreeNode)node).CeddDiscriptor;
            double dist = Cedd.Compare(ceddDiscriptor1, ceddDiscriptor2);
            return Convert.ToInt32(dist);
        }
    }
}
