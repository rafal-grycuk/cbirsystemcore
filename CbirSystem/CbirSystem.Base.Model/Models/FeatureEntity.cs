﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CbirSystem.Base.Model.Models
{
    public class FeatureEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public byte[] FeatureVector { get; set; }

        public int? GroupId { get; set; }

        [Required]
        public Indexer IndexerType { get; set; }

        [ForeignKey("ImageId")]
        public Image Image { get; set; }

        [Required]
        public int ImageId { get; set; }

    }
}
