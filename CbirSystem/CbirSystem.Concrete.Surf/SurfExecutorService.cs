﻿using System.Collections.Generic;
using Accord.Imaging;
using Accord.MachineLearning;
using CbirSystem.Base.Logic;
using System.Linq;
using CbirSystem.Utilities;

namespace CbirSystem.Concrete.Surf
{
    public class SurfExecutorService : AbstractExecutorService<SpeededUpRobustFeaturePoint>
    {
        private readonly IDictionary<Base.Model.Models.Image, Dictionary<int?, double>> _histogramsDictionary;
        private readonly EditableKeyValuePair<int, double[][]> _clusters;

        public SurfExecutorService(AbstractFeatureIndexerService<SpeededUpRobustFeaturePoint> indexer,
                               IFeatureExtractorService<SpeededUpRobustFeaturePoint> extractor,
                               IDictionary<Base.Model.Models.Image, Dictionary<int?, double>> histogramsDictionary,
                               EditableKeyValuePair<int, double[][]> clusters
                               ) : base(indexer, extractor)
        {
            _clusters = clusters;
            _histogramsDictionary = histogramsDictionary;
        }

        public override IList<Base.Model.Models.Image> ExecuteQuery(Base.Model.Models.Image query, params object[] parameters)
        {
            var queryFeatures = FeatureExtractor.ExtractFeatures(query).Result;
            double[][] queryFeaturesMatrix = (double[][])SurfFeatureExtractorService.GetFeatureMatrix(queryFeatures);
            var clusterCollection = new KMeansClusterCollection(_clusters.Key, new Accord.Math.Distances.Euclidean())
            {
                Centroids = _clusters.Value
            };
            int[] clusterFeaturesLabels = clusterCollection.Decide(queryFeaturesMatrix);
            for (int i = 0; i < clusterFeaturesLabels.Length; i++)
                queryFeatures[i].GroupId = clusterFeaturesLabels[i];

            var hist = queryFeatures
                .GroupBy(qf => qf.GroupId)
                .OrderBy(x => x.Key)
                .Select(h => new { GroupId = h.Key, Bin = (double)h.Count() }).ToDictionary(x => x.GroupId, x => x.Bin);
            var maxCount = _histogramsDictionary.Values.First().Max(x => x.Key);
            for (int i = 0; i <= maxCount; i++)
            {
                if (!hist.ContainsKey(i))
                    hist.Add(i, 0);
            }

            double[] queryMatrix = hist.Values.ToArray();
            foreach (var kvp in _histogramsDictionary)
                kvp.Key.Distance = Accord.Math.Distance.Euclidean(kvp.Value.Values.ToArray(), queryMatrix);

            if (parameters.Any() && parameters[0] != null && !string.IsNullOrWhiteSpace(parameters[0].ToString()) && int.TryParse(parameters[0].ToString(), out int threshold)) { }
            else
                threshold = 2;

            var retrivedImages = _histogramsDictionary
                                    .OrderBy(x => x.Key.Distance)
                                    .Where(ri => ri.Key.Distance <= threshold)
                                    .Select(ri => ri.Key).ToList();
            return retrivedImages;
        }
    }
}
