﻿using System;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    class RgbToHsv
    {
        public int[] ApplyFilter(int red, int green, int blue)
        {
            int[] results = new int[3];
            int h =0,s,v;
            double maxHsv = Math.Max(red, Math.Max(green, blue));
            double minHsv = Math.Min(red, Math.Min(green, blue));
            v = (int)(maxHsv);
            s = 0;
            if (maxHsv != 0)
                s = (int)(255 - 255 * (minHsv / maxHsv));

            if (maxHsv != minHsv)
            {
                int integerMaxHsv = (int)(maxHsv);
                if (integerMaxHsv == red && green >= blue)
                {
                    h = (int)(60 * (green - blue) / (maxHsv - minHsv));
                }
                else if (integerMaxHsv == red && green < blue)
                {
                    h = (int)(359 + 60 * (green - blue) / (maxHsv - minHsv));
                }
                else if (integerMaxHsv == green)
                {
                    h = (int)(119 + 60 * (blue - red) / (maxHsv - minHsv));
                }
                else if (integerMaxHsv == blue)
                {
                    h = (int)(239 + 60 * (red - green) / (maxHsv - minHsv));
                }
            }
            else h = 0;

            results[0] = h;
            results[1] = s;
            results[2] = v;

            return (results);
        }

    }
}