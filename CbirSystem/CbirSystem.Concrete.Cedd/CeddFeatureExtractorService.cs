﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using CbirSystem.Utilities;
using DataAccessLayer.Core.Interfaces.UoW;

namespace CbirSystem.Concrete.Cedd
{
    public class CeddFeatureExtractorService : IFeatureExtractorService<double[]>
    {
        private readonly IUnitOfWork _uow;
        public CeddFeatureExtractorService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public async Task<IList<Feature<double[]>>> ExtractFeatures(Base.Model.Models.Image image)
        {
            return await Task.Run(() =>
            {
                IList<Feature<double[]>> featureMatrix = new List<Feature<double[]>>();
                CEDD.Cedd cedd = new CEDD.Cedd();
                double[] ceddDiscriptor;
                using (Bitmap bmp = image)
                {
                    ceddDiscriptor = cedd.Apply(bmp);
                }
                Feature<double[]> feature = new Feature<double[]>()
                {
                    Data = ceddDiscriptor,
                    ImageId = image.Id,
                    IndexerType = Indexer.Cedd
                };
                featureMatrix.Add(feature);
                return featureMatrix;
            });
        }

        public async Task<IList<Feature<double[]>>> ExtractAllFeatures(IList<Base.Model.Models.Image> images)
        {
            return await Task.Run(() =>
            {
                List<Feature<double[]>> featureMatrix = new List<Feature<double[]>>();
                for (int i = 0; i < images.Count(); i++)
                {
                    var imageFeatures = ExtractFeatures(images[i]).Result;
                    featureMatrix.AddRange(imageFeatures);
                }
                var featureEntities = ToFeatureMatrixEntity(featureMatrix);
                _uow.Repository<FeatureEntity>().AddRange(featureEntities);
                _uow.Save();
                return featureMatrix;
            });
        }
        public static IList<Feature<double[]>> ToFeatureMatrix(ICollection<FeatureEntity> featureMatrixEntity)
        {
            IList<Feature<double[]>> featureMatrix = featureMatrixEntity.Select(fr => new Feature<double[]>()
            {
                Data =  ByteExtensions.ToDoubleArray(fr.FeatureVector),
                IndexerType = fr.IndexerType,
                GroupId = fr.GroupId,
                ImageId = fr.ImageId
            }).ToList();
            return featureMatrix;
        }

        public static IList<FeatureEntity> ToFeatureMatrixEntity(IList<Feature<double[]>> features)
        {
            IList<FeatureEntity> featureMatrixEntity = features.Select(fr => new FeatureEntity
            {
                FeatureVector = ByteExtensions.ToByteArray(fr.Data),
                IndexerType = fr.IndexerType,
                GroupId = fr.GroupId,
                ImageId = fr.ImageId
            }).ToList();
            return featureMatrixEntity;
        }

    }
}
