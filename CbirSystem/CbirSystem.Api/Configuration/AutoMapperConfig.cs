﻿using AutoMapper;
using CbirSystem.Base.Model.Identity;
using CbirSystem.Dto;
using CbirSystem.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace CbirSystem.Api.Configuration
{
    public static class AutoMapperConfig
    {
        public static IMapperConfigurationExpression Mapping(this IMapperConfigurationExpression configurationExpression, UserManager<User> userManager)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<User, BaseUserVm>()
                    .ForMember(dest => dest.Roles, member => member.MapFrom(src =>
                        userManager.GetRolesAsync(src).Result
                    ));
                cfg.CreateMap<RegisterUserDto, User>();
                
            });
            return configurationExpression;
        }



    }
}
