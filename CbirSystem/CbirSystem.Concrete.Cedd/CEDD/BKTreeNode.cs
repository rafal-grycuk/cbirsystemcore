﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CbirSystem.Concrete.Cedd.CEDD
{
    public abstract class BkTreeNode
    {
        public SortedDictionary<Int32, BkTreeNode> Children { get; set; }

        public virtual void Add(BkTreeNode node)
        {
            if (Children == null)
                Children = new SortedDictionary<Int32, BkTreeNode>();

            Int32 distance = CalculateDistance(node);
            if (Children.ContainsKey(distance))
            {
                Children[distance].Add(node);
            }
            else
            {
                Children.Add(distance, node);
            }
        }

        public virtual Int32 FindBestMatch(BkTreeNode node, Int32 bestDistance, out BkTreeNode bestNode)
        {
            Int32 distanceAtNode = CalculateDistance(node);
            bestNode = node;
            if (distanceAtNode < bestDistance)
            {
                bestDistance = distanceAtNode;
                bestNode = this;
            }
            Int32 possibleBest = bestDistance;
            foreach (Int32 distance in Children.Keys)
            {
                if (distance < distanceAtNode + bestDistance)
                {
                    possibleBest = Children[distance].FindBestMatch(node, bestDistance, out bestNode);
                    if (possibleBest < bestDistance)
                    {
                        bestDistance = possibleBest;
                    }
                }
            }
            return bestDistance;
        }

        public virtual void Query(BkTreeNode node, Int32 threshold, Dictionary<BkTreeNode, Int32> collected)
        {
            Int32 distanceAtNode = CalculateDistance(node);
            if (distanceAtNode == threshold)
            {
                collected.Add(this, distanceAtNode);
                return;
            }
            if (distanceAtNode < threshold)
            {
                collected.Add(this, distanceAtNode);
            }
            for (Int32 distance = (distanceAtNode - threshold); distance <= (threshold + distanceAtNode); distance++)
            {
                if (Children != null)
                {
                    if (Children.ContainsKey(distance))
                    {
                        Children[distance].Query(node, threshold, collected);
                    }
                }
            }
        }

        protected abstract int CalculateDistance(BkTreeNode node);
    }
}
