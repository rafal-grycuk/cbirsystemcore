﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CbirSystem.Base.Model.Models;

namespace CbirSystem.Base.Logic
{
    public abstract class AbstractExecutorService<TFeatureType> : IExecutorService
    {
        protected readonly AbstractFeatureIndexerService<TFeatureType> _featureIndexer;
        protected readonly IFeatureExtractorService<TFeatureType> _featureExtractor;
        public IFeatureExtractorService<TFeatureType> FeatureExtractor => _featureExtractor;
       
        public AbstractExecutorService(AbstractFeatureIndexerService<TFeatureType> indexer, IFeatureExtractorService<TFeatureType> extractor)
        {
            _featureIndexer = indexer;
            _featureExtractor = extractor;
        }
        public abstract IList<Image> ExecuteQuery(Image query, params object[] parameters);

        public async Task CreateIndex(params object[] parameters)
        {
            await _featureIndexer.CreateIndex(parameters);
        }

        public void DeleteIndex()
        {
            _featureIndexer.DeleteIndex();
        }
    }
}
