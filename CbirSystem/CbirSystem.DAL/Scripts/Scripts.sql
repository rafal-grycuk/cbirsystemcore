--Pascal
SELECT TOP 1000 [Id]
      ,[Extension]
      ,[ImageName]
      ,[RawImage]
      ,[Tag]
  FROM [CbirSystem_Pascal].[dbo].[Images]



  select tag, Count(*) as "Count"
  FROM [CbirSystem_Pascal].[dbo].[Images]
  group by tag;

  select Count(*)
   FROM [CbirSystem_Pascal].[dbo].[Images]


   -- Corel
   SELECT TOP 1000 [Id]
      ,[Extension]
      ,[ImageName]
      ,[RawImage]
      ,[Tag]
  FROM [CbirSystem_Corel].[dbo].[Images]



  select tag, Count(*) as "Count"
  FROM [CbirSystem_Corel].[dbo].[Images]
  group by tag;

  select Count(*)
   FROM [CbirSystem_Corel].[dbo].[Images]

   -- Microsoft Unanotated 
   SELECT TOP 1000 [Id]
      ,[Extension]
      ,[ImageName]
      ,[RawImage]
      ,[Tag]
  FROM [CbirSystem_MSUanotated].[dbo].[Images]



  select tag, Count(*) as "Count"
  FROM [CbirSystem_MSUanotated].[dbo].[Images]
  group by tag;

  select Count(*)
   FROM [CbirSystem_MSUanotated].[dbo].[Images]