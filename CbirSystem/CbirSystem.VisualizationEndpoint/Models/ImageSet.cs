﻿using CbirSystem.VisualizationEndpoint.Filters;

namespace CbirSystem.VisualizationEndpoint.Models
{
    public enum ImageSet
    {
        [PathInfo(@"ImageDatasets\Pascal")]
        Pascal,

        [PathInfo(@"ImageDatasets\CorelDb")]
        Corel,

        [PathInfo(@"ImageDatasets\MicrosoftUnanotated")]
        MicrosoftUnanotated
    }
}
