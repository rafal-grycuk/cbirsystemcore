﻿using CbirSystem.Base.Logic;
using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.Extensions.Logging;

namespace CbirSystem.Api.Controllers
{
    public class CeddIndexerController : IndexerController<double[]>
    {
        public CeddIndexerController(IUnitOfWork uow,
                                     ILoggerFactory loggerFactory,
                                     AbstractExecutorService<double[]> executor) : base(uow, loggerFactory, executor) { }
    }
}
