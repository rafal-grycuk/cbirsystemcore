﻿using System;
using System.Reflection;
using CbirSystem.ConsoleEndpoint.Filters;

namespace CbirSystem.ConsoleEndpoint.Extensions
{
    public static class EnumExtensions
    {
        public static string GetPath(Enum value)
        {
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null)
                return null;
            var attribute = (PathInfo)fieldInfo.GetCustomAttribute(typeof(PathInfo));
            return attribute.Path;
        }
    }
}
