﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CbirSystem.Base.Model.Identity;
using CbirSystem.Dto;
using CbirSystem.Utilities;
using CbirSystem.ViewModels;
using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace CbirSystem.Api.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;

        public AccountController(IUnitOfWork uow,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IOptions<JwtIssuerOptions> jwtOptions,
            ILoggerFactory loggerFactory
        )
            : base(uow, loggerFactory)
        {
            _userManager = userManager;
            _jwtOptions = jwtOptions.Value;
            _signInManager = signInManager;

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromForm] RegisterUserDto dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var entity = Mapper.Map<User>(dto);
                    entity.Email = dto.UserName + "@iisi.pcz.pl";
                    var result = await _userManager.CreateAsync(entity, dto.Password);
                    if (!result.Succeeded)
                    {
                        return StatusCode(500, result.Errors.GetErrorsString());
                    }
                    else
                    {
                        IdentityResult addToRoleResult = await _userManager.AddToRolesAsync(entity, new[] { "Readers", "Writers" });
                        if (addToRoleResult.Succeeded)
                            return Ok("User created successfully");
                        else
                        {
                            return StatusCode(500, result.Errors.GetErrorsString());
                        }
                    }
                }
                else
                {
                    return StatusCode(500, "Invalid data model.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occured ", ex, ex.InnerException);
                return StatusCode(500, ex.Message + " Inner Message " + ex.InnerException?.Message);
            }
        }

        [HttpPost("[action]")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AddUserToRole([FromForm]UserToRoleDto dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByNameAsync(dto.UserName);
                    if (user != null)
                    {
                        var result = await _userManager.AddToRoleAsync(user, dto.Role);
                        if (result.Succeeded)
                            return Ok(new { success = true });
                        else
                            return StatusCode(500, result.Errors.GetErrorsString());
                    }
                    else
                        return StatusCode(500, "User not exists.");
                }
                else
                {
                    return StatusCode(500, "Invalid data model.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occured ", ex, ex.InnerException);
                return StatusCode(500, ex.Message + " Inner Message " + ex.InnerException?.Message);
            }
        }

        [HttpPost("[action]")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DetachUserFromRole([FromForm]UserToRoleDto dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByNameAsync(dto.UserName);
                    if (user != null)
                    {
                        var result = await _userManager.RemoveFromRoleAsync(user, dto.Role);
                        if (result.Succeeded)
                            return Ok(new { success = true });
                        else
                            return StatusCode(500, result.Errors.GetErrorsString());
                    }
                    else
                        return StatusCode(500, "User not exists.");
                }
                else
                {
                    return StatusCode(500, "Invalid data model.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception occured ", ex, ex.InnerException);
                return StatusCode(500, ex.Message + " Inner Message " + ex.InnerException?.Message);
            }
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public IActionResult Login([FromForm] LoginUserVm applicationUser)
        {
            if (string.IsNullOrWhiteSpace(applicationUser.Login) || string.IsNullOrWhiteSpace(applicationUser.Password))
            {
                _logger.LogInformation(
                    $"Login or password are empty.");
                return BadRequest("Invalid credentials");
            }
            var result = _signInManager
                .PasswordSignInAsync(applicationUser.Login, applicationUser.Password, true, false).Result;
            if (result.Succeeded == false)
            {
                _logger.LogInformation(
                    $"Invalid username ({applicationUser.Login}) or password ({applicationUser.Password})");
                return BadRequest("Invalid credentials");
            }
            int tokenExpirationMinutes = 20;
#if DEBUG
            tokenExpirationMinutes = 150000;
#endif
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, applicationUser.Login),
                new Claim(JwtRegisteredClaimNames.Nbf,
                    new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp,
                    ((long)((DateTime.Now.AddMinutes(tokenExpirationMinutes) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)).ToString())
            };

            var token = new JwtSecurityToken(
                new JwtHeader(new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey)),
                    SecurityAlgorithms.HmacSha256)),
                new JwtPayload(claims));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

            var response = new
            {
                access_token = encodedJwt,
                expires_in = token.ValidTo.ToString("yyyy-MM-ddTHH:mm:ss")
            };
            var json = JsonConvert.SerializeObject(response, _serializerSettings);
            return new OkObjectResult(json);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var authenticatedUser = ((ClaimsIdentity)User?.Identity)?.Claims?.First()?.Value;
            if (authenticatedUser != null)
            {
                var user = _uow.Repository<User>().Get(u => u.UserName == authenticatedUser, false);
                if (user != null)
                {
                    var userIndividualVm = Mapper.Map<BaseUserVm>(user);
                    return Ok(userIndividualVm);
                }
                else
                    return StatusCode(500, "Given user does not exist in the database.");

            }
            else return Unauthorized();
        }


    }
}
