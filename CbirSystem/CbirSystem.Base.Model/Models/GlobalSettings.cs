﻿namespace CbirSystem.Base.Model.Models
{
    public class GlobalSettings
    {
        public string ImageSet { get; set; }
        public string SlackWebHook { get; set; }
    }
}
