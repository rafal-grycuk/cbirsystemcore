﻿using System;
using System.Linq;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Models;
using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CbirSystem.Api.Controllers
{
    public class SimulatorController : BaseApiController
    {
        private readonly ISimulationEvaluatorService<RetrievalFactors> _simulationEvaluatorService;
        private readonly GlobalSettings _globalSettings;
        public SimulatorController(IUnitOfWork uow,
                                   ILoggerFactory loggerFactory,
                                   ISimulationEvaluatorService<RetrievalFactors> simulationEvaluatorService,
                                   IOptions<GlobalSettings> globalSettings
            ) : base(uow, loggerFactory)
        {
            _globalSettings = globalSettings?.Value;
            _simulationEvaluatorService = simulationEvaluatorService;
        }

        [HttpPost("[Action]")]
        public IActionResult Simulate([FromForm] double? distance = null)
        {
            try
            {
                DateTime start = DateTime.Now;
                var files = Request?.Form?.Files;
                if (files?.Count > 0)
                {
                    var queryImages = files.Select(file => new Image
                    {
                        Extension = file.FileName.Substring(file.FileName.IndexOf('.'),
                            file.FileName.Length - file.FileName.IndexOf('.')),
                        ImageName = System.IO.Path.GetFileName(file.FileName),
                        RawImage = Image.ImageToByte((System.Drawing.Bitmap)System.Drawing.Image.FromStream(file.OpenReadStream())),
                        Tag = file.Name
                    }).ToList();
                    var results = _simulationEvaluatorService.SimulateMultiQuery(queryImages, distance);
                    var simulationResults = new SimulationResults()
                    {
                        RetrievalFactors = results,
                        AvgPrecision = Math.Round(results.Average(ret => ret.Precision), 1),
                        AvgRecall = Math.Round(results.Average(ret => ret.Recall), 1)
                    };
                    DateTime end = DateTime.Now;
                    TimeSpan computationTime = (end - start);
                    _logger.LogWarning($"Simulation of set: {_globalSettings.ImageSet} was finished with Avg Precision: {simulationResults.AvgPrecision} and Avg Recall: {simulationResults.AvgRecall}. The computation time is equal {computationTime}");

                    return Ok(simulationResults);
                }
                else
                    return StatusCode(500, "No files selected.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return StatusCode(500, "Error occured");
            }
        }

    }
}
