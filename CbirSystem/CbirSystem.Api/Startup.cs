﻿using System;
using System.Collections.Generic;
using System.Text;
using Accord.Imaging;
using Accord.MachineLearning;
using CbirSystem.Api.Configuration;
using CbirSystem.Base.Logic;
using CbirSystem.Base.Model.Identity;
using CbirSystem.Base.Model.Models;
using CbirSystem.Concrete.Cedd;
using CbirSystem.Concrete.Cedd.CEDD;
using CbirSystem.Concrete.Surf;
using CbirSystem.DAL.EF;
using CbirSystem.Utilities;
using DataAccessLayer.Core.EntityFramework.Repositories;
using DataAccessLayer.Core.EntityFramework.UoW;
using DataAccessLayer.Core.Interfaces.Repositories;
using DataAccessLayer.Core.Interfaces.UoW;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Slack;
using Microsoft.IdentityModel.Tokens;

namespace CbirSystem.Api
{
    public class Startup
    {
        private readonly string _connectionString;
        //cedd
        private readonly BkTree<CeddTreeNode> _ceddTree;

        //SURF
        private readonly IDictionary<Base.Model.Models.Image, Dictionary<int?, double>> _histogramsDictionary;
        private readonly EditableKeyValuePair<int, double[][]> _clusters;
        public IHostingEnvironment HostingEnvironment { get; set; }

        public IConfigurationRoot Configuration { get; }
        public IServiceCollection Services { get; private set; }
        public Startup(IHostingEnvironment env)
        {
            _ceddTree = new BkTree<CeddTreeNode>();
            _histogramsDictionary = new Dictionary<Base.Model.Models.Image, Dictionary<int?, double>>();
            _clusters = new EditableKeyValuePair<int, double[][]>();
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var imageSet = Configuration.GetSection("GlobalSettings:ImageSet");
            if (env.IsEnvironment("Development"))
            {
                builder.AddApplicationInsightsSettings(developerMode: true);
                builder.AddUserSecrets<Startup>();
            }
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            if (env.IsDevelopment())
            {
                _connectionString = Configuration[imageSet.Value + "Connection"]; // SQL SERVER
                //_connectionString = Configuration["MySqlConnection"]; // My SQL
            }
            else if (env.IsProduction())
            {
                _connectionString = Configuration.GetConnectionString(imageSet.Value + "Connection"); // SQL SERVER
                //_connectionString = Configuration.GetConnectionString("MySqlConnection"); // My SQL
            }
            HostingEnvironment = env;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Framework Services
            // Add framework services.
            services.Configure<JwtIssuerOptions>(options => Configuration.GetSection("JwtIssuerOptions").Bind(options));
            services.Configure<GlobalSettings>(options => Configuration.GetSection("GlobalSettings").Bind(options));
            services.AddOptions();
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddDbContext<CbirDbContext<User, Role, int>>(options =>
            {
                options.UseSqlServer(_connectionString);  // SQL SERVER
                // options.UseMySql(_connectionString); // My SQL
            });

            services.AddIdentity<User, Role>(o =>
                {
                    o.Password.RequireDigit = false;
                    o.Password.RequireUppercase = false;
                    o.Password.RequireLowercase = false;
                    o.Password.RequireNonAlphanumeric = false;
                    o.User.RequireUniqueEmail = false;
                })
                .AddEntityFrameworkStores<CbirDbContext<User, Role, int>>()
                .AddDefaultTokenProviders();

            //services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");

            //var corsBuilder = new CorsPolicyBuilder();
            //corsBuilder.AllowAnyHeader();
            //corsBuilder.WithMethods("GET", "POST", "PUT", "DELETE");
            //corsBuilder.WithOrigins("http://iisi.pcz.pl", "http://www.iisi.pcz.pl", "http://localhost:4200");
            //corsBuilder.AllowCredentials();

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowCors", corsBuilder.Build());
            //});

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = long.MaxValue;
                x.KeyLengthLimit = int.MaxValue;
            });
            services.AddMvc();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Jwt";
                options.DefaultChallengeScheme = "Jwt";
            }).AddJwtBearer("Jwt", options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtIssuerOptions:SecretKey"])),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(5)
                };
            });
            #endregion

            #region Our Services
            var cs = new ConnectionStringDto() { ConnectionString = _connectionString };
            services.AddSingleton(cs);
            services.AddSingleton(_ceddTree);
            services.AddSingleton(_clusters);
            services.AddSingleton(_histogramsDictionary);
            services.AddScoped<DbContext, CbirDbContext<User, Role, int>>();
            services.AddScoped<DbContextOptions<CbirDbContext<User, Role, int>>>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<SpeededUpRobustFeaturesDetector, SpeededUpRobustFeaturesDetector>();
            // -----SURF
            services.AddScoped<IFeatureExtractorService<SpeededUpRobustFeaturePoint>, SurfFeatureExtractorService>();
            services.AddScoped<AbstractFeatureIndexerService<SpeededUpRobustFeaturePoint>, SurfFeatureIndexerService>();
            services.AddScoped<AbstractExecutorService<SpeededUpRobustFeaturePoint>, SurfExecutorService>();

            //-----CEDD
            services.AddScoped<IFeatureExtractorService<double[]>, CeddFeatureExtractorService>();
            services.AddScoped<AbstractFeatureIndexerService<double[]>, CeddFeatureIndexerService>();
            services.AddScoped<AbstractExecutorService<double[]>, CeddExecutorService>();

            // the Executor Serivce Injection - defines which executor will be used.
             services.AddScoped<IExecutorService, CeddExecutorService>();
            //services.AddScoped<IExecutorService, SurfExecutorService>();

            services.AddScoped<ISimulationEvaluatorService<RetrievalFactors>, SimulationEvaluatorService>();

            var mappingConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.Mapping(services.BuildServiceProvider().GetService<UserManager<User>>());
            });

            services.AddSingleton(x => mappingConfig.CreateMapper());
            services.AddScoped<IRepository<User>, EntityFrameworkRepository<User>>();
            services.AddScoped<IRepository<Role>, EntityFrameworkRepository<Role>>();
            services.AddScoped<IRepository<IdentityUserRole<int>>, EntityFrameworkRepository<IdentityUserRole<int>>>();
            #endregion
            Services = services;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddConsole();
            loggerFactory.AddFile("Logs/CbirSystem-{Date}.txt");
            var slackWebHook = Configuration.GetSection("GlobalSettings:SlackWebHook").Value;
            var configuration = new SlackConfiguration()
            {
                WebhookUrl = new Uri(slackWebHook),
                MinLevel = LogLevel.Info
            };
            loggerFactory.AddSlack(configuration, env);

            //app.UseCors("AllowCors");

            var dbContext = Services.BuildServiceProvider().GetRequiredService<DbContext>();
            dbContext.Database.Migrate();
            var userManager = Services.BuildServiceProvider().GetRequiredService<UserManager<User>>();
            var roleManager = Services.BuildServiceProvider().GetRequiredService<RoleManager<Role>>();
            Seed.RunSeed(dbContext, roleManager, userManager);

            app.UseAuthentication();

            app.UseMvcWithDefaultRoute();
            
                app.Run(async (context) =>
            {
                context.Features.Get<IHttpMaxRequestBodySizeFeature>().MaxRequestBodySize = 100_000_000;
                context.Response.StatusCode = 404;
                await context.Response.WriteAsync("Page not found");
            });

        }
    }
}
